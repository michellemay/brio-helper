<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/admin
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $brio_helper    The ID of this plugin.
	 */
	private $brio_helper;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.13
	 * @param      string    $brio_helper       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $brio_helper, $version ) {

		$this->brio_helper = $brio_helper;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.13
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Brio_Helper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Brio_Helper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( 'font-awesome', 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
		wp_enqueue_style( plugins_url( 'assets/css/admin.min.css', dirname(__FILE__) ), array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.13
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Brio_Helper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Brio_Helper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->brio_helper, plugins_url( 'assets/js/admin.min.js', dirname(__FILE__)), array( 'jquery' ), $this->version, false );
		//wp_enqueue_script( $this->brio_helper, plugin_dir_url( __FILE__ ) . 'js/cust.js', array( 'jquery' ), $this->version, false );

		global $pagenow, $wp_customize;
		if ('widgets.php' === $pagenow || isset($wp_customize)) {
			wp_enqueue_media();
			wp_enqueue_script('brio_image_upload',  plugins_url( 'assets/js/image-upload.min.js', dirname(__FILE__)), array('jquery'), $this->version, false );
		}

	}

	public function enqueue_notices() {
		/**
		*
		*
		*
		*
		*/


		if (current_user_can('manage_options')) {
			if (isset($_POST['brio_new_install_notice_dismissed'])) {
				update_option('brio_new_install_notice', 1);
			}
		}

		if (get_option('brio_new_install_notice') || !current_user_can('manage_options')) {
			return;
		}

		$this_theme = wp_get_theme();
		$theme_name = $this_theme->get('Name');

		?>
		<div class="notice notice-warning is-dismissible">
			<h2><?php _e('Hello!', 'brio-helper'); ?></h2>
			<p>Thank you for installing <strong><?php echo $theme_name ?></strong>.</p>
			<p>If you need help setting up your theme, check out our Quickstart Guide that covers everything you need to get up and running.</p>
			<p>Already setup? Click the button below to hide this notice:</p>
			<form action="<?php echo admin_url(); ?>" method="post">
				<?php wp_nonce_field('brio-new-install-notice-nonce'); ?>
				<input type="hidden" value="true" name="brio_new_install_notice_dismissed" />
				<p class="submit" style="margin-top: 5px; padding-top: 5px;">
					<input name="submit" class="button" value="Hide this notice" type="submit" />
				</p>
			</form>
		</div>
		<?php

	}

}
