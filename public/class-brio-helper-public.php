<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/public
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $brio_helper    The ID of this plugin.
	 */
	private $brio_helper;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.13
	 * @param      string    $brio_helper       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $brio_helper, $version ) {

		$this->brio_helper = $brio_helper;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.13
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Brio_Helper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Brio_Helper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->brio_helper, plugins_url( 'assets/css/main.min.css', dirname(__FILE__) ), array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.13
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Brio_Helper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Brio_Helper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->brio_helper, plugins_url( 'assets/js/scripts.min.js', dirname(__FILE__)), array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->brio_helper, 'simpleLikes', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'like' => __( 'Like', 'brio-helper' ),
		'unlike' => __( 'Unlike', 'brio-helper' )
	) ); 
	}

}
