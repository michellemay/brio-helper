<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://brioblogstudio.com
 *
 * @wordpress-plugin
 * Plugin Name:       Brio Helper
 * Plugin URI:        https://brioblogstudio.com
 * Description:       Settings and features to accompany themes purchased with Brio Blog Studio
 * Version:           0.15
 * Author:            Brio Blog Studio
 * Author URI:        https://brioblogstudio.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       brio
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}



/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-brio-helper-activator.php
 */
function activate_brio_helper() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-brio-helper-activator.php';
	Brio_Helper_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-brio-helper-deactivator.php
 */
function deactivate_brio_helper() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-brio-helper-deactivator.php';
	Brio_Helper_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_brio_helper' );
register_deactivation_hook( __FILE__, 'deactivate_brio_helper' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-brio-helper.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.13
 */
function run_brio_helper() {

	$this_theme = wp_get_theme();
	$theme_textdomain = $this_theme->get('TextDomain');
	if ($this_theme->get('Author') != 'Brio Blog Studio') {
		$this_theme_child = wp_get_theme( get_template() )->get( 'Author' );
		if ($this_theme_child != 'Brio Blog Studio') {
			return;
		} else {
			if ($theme_textdomain) {
				$theme_textdomain = explode('-', trim($theme_textdomain));
				if ($theme_textdomain[0] != 'brio-helper') {
					return;
				}
			} 
		}
	}

	$plugin = new Brio_Helper();
	$plugin->run();

}

run_brio_helper();

/**
 * Check for updates
**/

function check_for_update() {

	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'http://brio.michellemay.co.uk/updates/brio.json',
		__FILE__,
		'brio-helper'
	);

}
check_for_update();
