=== Brio Helper ===
Contributors: Brio Blog Studio
Donate link: https://brioblogstudio.com
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 0.15
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Settings and features to accompany themes purchased with Brio Blog Studio

== Installation ==

1. Upload `brio-helper.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= How do I use the shortcodes? =

Insert the following into the wp text editor to allow a two column layout:

`[brio_left] This text goes to the left [/brio_left]`
`[brio_right] This text goes to the left [/brio_right]`

Insert the following into the wp text editor to display your social icons:

`[brio_social_icons]`

You must ensure that your [Social Links]()/wp-admin/admin.php?page=brio-social-links) settings are populated.

= How do the theme hooks work? =

Your theme should already have the following actions (because you should be using a Brio made theme)

`do_action('brio_after_first_post');`
`do_action('brio_content_start');`
`do_action('brio_content_end');`

These will assist the insertion of [code/content](/wp-admin/admin.php?page=brio-hooks).

= Where do I insert custom CSS that can override the theme? =

[Here](/wp-admin/admin.php?page=brio-css).

= What widgets does this plugin add? =

* Instagram 
* About Me
* Promo Box
* Popular Posts
* Recent Posts

= What new Customizer options are available to me? =

You can perform the following additional actions [here](/wp-admin/customize.php):

* Social Navigation - Add your social media links and display them in the header of your blog. You can also display these anywhere on your site with our handy shortcode.
* Post Settings - Display related posts
* Sharing - Add lightweight sharing buttons to your posts
* Author Profile - Display the profile of your post author at the bottom of each blog psot
* Featured Category Posts - Choose 4 categories to display the latest post from before the start of your content


== Changelog ==

= 0.15 =
* Beta release
