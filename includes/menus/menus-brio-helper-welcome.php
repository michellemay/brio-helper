<?php

if (! defined('ABSPATH')) {
    exit;
}

class Brio_Helper_Menus_Welcome
{
    public static function welcome_options_page()
    {
        ?>

	<div class="wrap">

	<h1><?php _e('Welcome', 'brio-helper'); ?></h1>

  <p>Lorem ipsum dolor sit amet, cu illud clita mel, ei eum congue veritus. In audiam tamquam adipiscing qui, laboramus deseruisse per at, at eros quaeque dolorum sed. Invidunt praesent ne pri. Fugit errem mea an. Eam ipsum putent reprimique ea. Possit euripidis vis et, mea detracto scripserit accommodare te.
</p>

</div> <!-- .wrap -->
<?php

    }
}
