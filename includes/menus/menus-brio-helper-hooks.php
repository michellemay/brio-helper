<?php

class Brio_Helper_Menus_Hooks{


    public static function settings()
    {
        register_setting('brio_hooks_page', 'brio_settings');

        add_settings_section(
            'brio_helper_hooks_page_section',
            __('Please be careful when adding code in these options. This can break your site if not added correctly!', 'brio-helper'),
            array('Brio_Helper_Menus_Hooks','settings_section_callback'),
            'brio_hooks_page'
            );

        add_settings_field(
            'brio_head_hook_input',
            __('Code to add to the  &lt;head&gt;', 'brio-helper').'<p style="font-style:normal;font-weight:normal;">'.__('For example, Google Analytics tracking code', 'brio-helper').'</p>',
            array('Brio_Helper_Menus_Hooks','head_hook_input_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );

        add_settings_field(
            'brio_body_top_hook_input',
            __('Code to add directly after the opening &lt;body&gt; tag', 'brio-helper'),
            array('Brio_Helper_Menus_Hooks','body_top_hook_input_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );

        add_settings_field(
            'brio_body_bottom_hook_input',
            __('Code to add just before the closing &lt;/body&gt; tag', 'brio-helper'),
            array('Brio_Helper_Menus_Hooks','body_bottom_hook_input_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );



        // conntent hooks from here
        add_settings_field(
            'brio_section_description', '<h2>Content Based Hooks</h2>',
            array('Brio_Helper_Menus_Hooks','section_description_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );


        // after first post on homepage/archives
        // add_settings_field(
        //     'brio_after_first_post_hook_input',
        //     __('After the first blog post on the homepage or archive', 'brio-helper').'<p style="font-style:normal;font-weight:normal;">'.__('For example, you may wish to place a banner ad after the first post', 'brio-helper').'</p>',
        //     array('Brio_Helper_Menus_Hooks','after_first_post_hook_input_render'),
        //     'brio_hooks_page',
        //     'brio_helper_hooks_page_section'
        //     );

        // beginning of each blog post content
        add_settings_field(
            'brio_entry_top_hook_input',
            __('Beginning of blog post content', 'brio-helper'),
            array('Brio_Helper_Menus_Hooks','brio_entry_top_hook_input_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );

        // end of each blog post content
        add_settings_field(
            'brio_entry_bottom_hook_input',
            __('End of blog post content', 'brio-helper'),
            array('Brio_Helper_Menus_Hooks','brio_entry_bottom_hook_input_render'),
            'brio_hooks_page',
            'brio_helper_hooks_page_section'
            );
    }




    public static function section_description_render()
    {
        ?>
        <hr style="margin-bottom: 20px;">
        <p style="width:1000px; max-width: 100%;">Use the fields below to add content to various positions on your site.<br />For example, you may wish to add an affiliate code/disclaimer to the beginning of all blog posts via the "Beginning of blog post content" section.</p>
        <?php

    }




    public static function head_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_head_hook_input]' placeholder=''><?php if (isset($options['brio_head_hook_input'])) {
            echo $options['brio_head_hook_input'];
        } ?></textarea>
        <?php

    }




    public static function body_bottom_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_body_bottom_hook_input]' placeholder=''><?php if (isset($options['brio_body_bottom_hook_input'])) {
            echo $options['brio_body_bottom_hook_input'];
        } ?></textarea>
        <?php

    }




    public static function body_top_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_body_top_hook_input]' placeholder=''><?php if (isset($options['brio_body_top_hook_input'])) {
            echo $options['brio_body_top_hook_input'];
        } ?></textarea>
        <?php

    }




    public static function after_first_post_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_after_first_post_hook_input]' placeholder=''><?php if (isset($options['brio_after_first_post_hook_input'])) {
            echo $options['brio_after_first_post_hook_input'];
        } ?></textarea>
        <?php

    }




    public static function brio_entry_top_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_entry_top_hook_input]' placeholder=''><?php if (isset($options['brio_entry_top_hook_input'])) {
            echo $options['brio_entry_top_hook_input'];
        } ?></textarea>
        <?php

    }



    public static function brio_entry_bottom_hook_input_render()
    {
        $options = get_option('brio_settings'); ?>
        <textarea style='width:1000px; max-width: 100%;' rows='8' name='brio_settings[brio_entry_bottom_hook_input]' placeholder=''><?php if (isset($options['brio_entry_bottom_hook_input'])) {
            echo $options['brio_entry_bottom_hook_input'];
        } ?></textarea>
        <?php

    }




    public static function settings_section_callback()
    {

        // description text
        echo '<p>'.__('Use the fields below to add custom code to the Head, Body or Footer of your site.', 'brio-helper').'</p>
        <p>'.__('These settings will be carried over if you install any other brio theme.', 'brio-helper').'</p>';
    }




    public static function hooks_options_page()
    {
        ?>
        <form action='options.php' method='post'>

         <h1>Theme Hooks</h1>

         <?php
         settings_fields('brio_hooks_page');
         do_settings_sections('brio_hooks_page');
         submit_button(); ?>

     </form>
     <?php

 }


 public static function user_hook_head()
    { // wp_head
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_head_hook_input'])) {
            $output .= '<!-- brio brio custom code head -->' . $options['brio_head_hook_input'] . '<!-- // brio brio custom code head -->';
        }
        echo $output;
    }





    public function user_hook_body_top()
    { // After opening <body> tag
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_body_top_hook_input'])) {
            $output .= '<!-- brio brio custom after <body> -->' . $options['brio_body_top_hook_input'] . '<!-- // brio brio custom after <body> -->';
        }
        echo $output;
    }


    public static function user_hook_body_bottom()
    { // wp_footer
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_body_bottom_hook_input'])) {
            $output .= '<!-- custom footer code -->' . $options['brio_body_bottom_hook_input'];
        }
        echo $output;
    }





    public function user_hook_after_first_post()
    { // After the first post (unless grid layout)
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_after_first_post_hook_input'])) {
            $output .= '<div class="hook_after-first-post"><!-- brio custom code after first post -->' . do_shortcode($options['brio_after_first_post_hook_input']) . '<!-- // brio brio custom code after first post --></div>';
        }
        echo $output;
    }





    public function user_hook_entry_top()
    { // top of post content
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_entry_top_hook_input'])) {
            $output .= do_shortcode($options['brio_entry_top_hook_input']);
        }
        echo $output;
    }




    public function user_hook_entry_bottom()
    { // end of post content
        $output = '';
        $options = get_option('brio_settings', '');
        if (!empty($options['brio_entry_bottom_hook_input'])) {
            $output .= do_shortcode($options['brio_entry_bottom_hook_input']);
        }
        echo $output;
    }
		/*
		Required theme styling for the above hook:

		style.css:
		.hook_after-first-post {
		    margin: 30px auto 50px;
		    text-align: center;
		}

		responsive.css:
		@media only screen and (max-width: 400px) {
		    .hook_after-first-post {
		        display: none;
		    }
		}
		*/


    }
