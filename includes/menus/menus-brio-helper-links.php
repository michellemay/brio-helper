<?php

class Brio_Helper_Menus_Links
{

    public static function settings()
    {
        register_setting('social_links_options_page', 'brio_social_links');

        add_settings_section(
        'social_links_options_page_section',
        '',
        array('Brio_Helper_Menus_Links','social_links_section_callback'),
        'social_links_options_page'
    );

        add_settings_field(
        'email',
        '<i class="fa fa-envelope"></i>&nbsp;&nbsp;Email',
        array('Brio_Helper_Menus_Links','brio_email_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'twitter',
        '<i class="fa fa-twitter"></i>&nbsp;&nbsp;Twitter',
        array('Brio_Helper_Menus_Links','brio_twitter_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'instagram',
        '<i class="fa fa-instagram"></i>&nbsp;&nbsp;Instagram',
        array('Brio_Helper_Menus_Links','brio_instagram_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'facebook',
        '<i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook',
        array('Brio_Helper_Menus_Links','brio_facebook_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'google_plus',
        '<i class="fa fa-google-plus"></i>&nbsp;&nbsp;Google Plus',
        array('Brio_Helper_Menus_Links','brio_google_plus_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'bloglovin',
        '<i class="fa fa-plus"></i>&nbsp;&nbsp;Bloglovin&#146;',
        array('Brio_Helper_Menus_Links','brio_bloglovin_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'pinterest',
        '<i class="fa fa-pinterest"></i>&nbsp;&nbsp;Pinterest',
        array('Brio_Helper_Menus_Links','brio_pinterest_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'snapchat',
        '<i class="fa fa-snapchat-ghost"></i>&nbsp;&nbsp;Snapchat',
        array('Brio_Helper_Menus_Links','brio_snapchat_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'youtube',
        '<i class="fa fa-youtube-play"></i>&nbsp;&nbsp;YouTube',
        array('Brio_Helper_Menus_Links','brio_youtube_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'tumblr',
        '<i class="fa fa-tumblr"></i>&nbsp;&nbsp;Tumblr',
        array('Brio_Helper_Menus_Links','brio_tumblr_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'flickr',
        '<i class="fa fa-flickr"></i>&nbsp;&nbsp;Flickr',
        array('Brio_Helper_Menus_Links','brio_flickr_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );

        add_settings_field(
        'rss',
        '<i class="fa fa-rss"></i>&nbsp;&nbsp;RSS Feed',
        array('Brio_Helper_Menus_Links','brio_rss_field_render'),
        'social_links_options_page',
        'social_links_options_page_section'
    );
    }


    public static function brio_email_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='email' name='brio_social_links[email]' placeholder='e.g. yourname@gmail.com' value="<?php if (isset($links['email'])) {
            echo $links['email'];
        } ?>"> <?php

    }

    public static function brio_twitter_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[twitter]' placeholder='e.g. http://twitter.com/brioblogstudio' value="<?php if (isset($links['twitter'])) {
            echo $links['twitter'];
        } ?>"> <?php

    }

    public static function brio_instagram_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[instagram]' placeholder='e.g. http://instagram.com/brioblogstudio' value="<?php if (isset($links['instagram'])) {
            echo $links['instagram'];
        } ?>"> <?php

    }

    public static function brio_facebook_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[facebook]' placeholder='e.g. http://facebook.com/brioblogstudio' value="<?php if (isset($links['facebook'])) {
            echo $links['facebook'];
        } ?>"> <?php

    }

    public static function brio_google_plus_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[google_plus]' placeholder='e.g. http://plus.google.com/+brio' value="<?php if (isset($links['google_plus'])) {
            echo $links['google_plus'];
        } ?>"> <?php

    }

    public static function brio_bloglovin_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[bloglovin]' placeholder='e.g. http://www.bloglovin.com/blogs/brio-3890264' value="<?php if (isset($links['bloglovin'])) {
            echo $links['bloglovin'];
        } ?>"> <?php
    }

    public static function brio_pinterest_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[pinterest]' placeholder='e.g. http://pinterest.com/brioblogstudio' value="<?php if (isset($links['pinterest'])) {
            echo $links['pinterest'];
        } ?>"> <?php

    }

    public static function brio_snapchat_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[snapchat]' placeholder='e.g. http://www.snapchat.com/add/brio.co' value="<?php if (isset($links['snapchat'])) {
            echo $links['snapchat'];
        } ?>"> <?php

    }

    public static function brio_youtube_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[youtube]' placeholder='e.g. http://www.youtube.com/user/brioblogstudio' value="<?php if (isset($links['youtube'])) {
            echo $links['youtube'];
        } ?>"> <?php

    }

    public static function brio_tumblr_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[tumblr]' placeholder='e.g. http://brioblogstudio.tumblr.com' value="<?php if (isset($links['tumblr'])) {
            echo $links['tumblr'];
        } ?>"> <?php

    }

    public static function brio_flickr_field_render()
    {
        $links = get_option('brio_social_links'); ?>
	<input class='large-text' type='url' name='brio_social_links[flickr]' placeholder='e.g. http://flickr.com/brioblogstudio' value="<?php if (isset($links['flickr'])) {
            echo $links['flickr'];
        } ?>"> <?php

    }

    public static function brio_rss_field_render()
    {
        $links = get_option('brio_social_links'); ?>
    <input class='large-text' type='url' name='brio_social_links[rss]' placeholder='e.g. https://brioblogstudio.com/rss' value="<?php if (isset($links['rss'])) {
            echo $links['rss'];
        } ?>"> <?php

    }

    public static function social_links_section_callback()
    {
        // description text
    echo '<p>'.__('Add the links to each social media page below:', 'brio-helper').'</p>';
    }




    public static function social_links_options_page()
    {
        ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style scoped>
	.form-table th {width: 120px;}
	</style>
	<form action='options.php' method='post'>

		<h1>Social Media Links</h1>

		<?php
        settings_fields('social_links_options_page');
        do_settings_sections('social_links_options_page');
        submit_button(); ?>
		</form>
	<?php

    }


}
