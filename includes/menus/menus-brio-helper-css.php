<?php

class Brio_Helper_Menus_CSS
{

    public static function settings()
    {
        register_setting('brio_css_page', 'brio_css');

        add_settings_section(
                'brio_brio_css_page_section',
                __('Custom CSS', 'brio-helper'),
                array('Brio_Helper_Menus_CSS','css_section_callback'),
                'brio_css_page'
            );

        add_settings_field(
                'brio_textarea_css',
                __('Any CSS added to this box will be kept after theme updates.', 'brio-helper'),
                array('Brio_Helper_Menus_CSS','textarea_css_render'),
                'brio_css_page',
                'brio_brio_css_page_section'
            );
    }


    public static function textarea_css_render()
    {
        $options = get_option('brio_css'); ?>

			<link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.18.2/codemirror.min.css" rel="stylesheet" />
			<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.18.2/theme/hopscotch.css" rel="stylesheet" /> -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.18.2/codemirror.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.18.2/mode/css/css.min.js"></script>

			<style scoped>
			.CodeMirror {
				height: 600px;
				width: 95%;
			}
			</style>

			<textarea style="" id="brio_custom_css" name="brio_css[brio_textarea_css]" placeholder="body {color: #000000; background: #ffffff}"><?php if (isset($options['brio_textarea_css'])) {
            echo $options['brio_textarea_css'];
        } ?></textarea>

			<script>
			jQuery(document).ready(function() {
	            var editor = CodeMirror.fromTextArea(document.getElementById("brio_custom_css"), {
	                lineNumbers: true,
	                mode: "text/css",
	                //theme: "hopscotch"
	            });
	        })
			</script>

			<?php

    }




    public static function css_section_callback()
    {
        //_e('Any CSS added to the box below will be kept after theme updates.', 'brio-helper');
    }




    public static function css_options_page()
    {
        ?>
			<form action='options.php' method='post'>

				<?php
          settings_fields('brio_css_page');
        	do_settings_sections('brio_css_page');
        	submit_button();
				?>

			</form>
			<h3><?php printf(__('Remember, you can also change the appearance of your site by using the <a href="%s">Customizer</a>.', 'brio-helper'), admin_url('customize.php')); ?></h3>
			<?php

    }



    static function css_head()
    { // wp_head
            $output = '';
        $options = get_option('brio_css', '');
        if (!empty($options['brio_textarea_css'])) {
            $output .= '<!-- brio custom css --><style>' . $options['brio_textarea_css'] . '</style><!-- // brio custom css -->';
        }
        echo $output;
    }
}
