<?php


class Brio_Helper_Menus_Shortcodes

{

    public static function shortcodes_options_page()
    {
        ?>
	<div class="wrap">


		<h1>Shortcodes</h1>

		<p>Lorem ipsum dolor sit amet, cu illud clita mel, ei eum congue veritus. In audiam tamquam adipiscing qui, laboramus deseruisse per at, at eros quaeque dolorum sed. Invidunt praesent ne pri. Fugit errem mea an. Eam ipsum putent reprimique ea. Possit euripidis vis et, mea detracto scripserit accommodare te.
</p>
		<h2>Our custom shortcodes:</h2>

		<div class="card">
      <h2>Left/Right Layouts</h2>
      <p><input class="widefat" type="text" value='[brio_half_left] This text goes to the left [/brio_half_left]' /></p>
			<p><input class="widefat" type="text" value='[brio_half_right] ...and this text goes to the right [/brio_half_right]' /></p>
    </div>

		<div class="card">
      <h2>Social Icons</h2>
			<p><input class="widefat" type="text" value="[brio_social_icons]" /></p>
			<p>The social icons shortcode will try to match the content around it. If you want to change this, the shortcode has two options, size and color. You can use one or both. Usage works as follows:</p>
			<p><input class="widefat" type="text" value="[brio_social_icons size=&#34;20px&#34; color=&#34;#7a7a7a&#34;]" /></p>
		</div>



	</div>

	<?php

    }
}
