<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.14
	 * @access   protected
	 * @var      Brio_Helper_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.14
	 * @access   protected
	 * @var      string    $brio_helper    The string used to uniquely identify this plugin.
	 */
	protected $brio_helper;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.14
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.14
	 */
	public function __construct() {

		$this->brio_helper = 'brio-helper';
		$this->version = '0.11';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		//$this->check_for_update();
		$this->load_widgets();
		$this->add_shortcodes();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Brio_Helper_Loader. Orchestrates the hooks of the plugin.
	 * - Brio_Helper_i18n. Defines internationalization functionality.
	 * - Brio_Helper_Admin. Defines all hooks for the admin area.
	 * - Brio_Helper_Public. Defines all hooks for the public side of the site.
	 * - Brio_Helper_Widgets. Defines all widgets.
	 * - Plugin Update Checker. Checks whether the plugin has a new version (outside of the WordPress server).
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-brio-helper-admin.php';


		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-widgets.php';

		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-shortcodes.php';

		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-functions.php';


		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/customizer/customizer-brio-helper.php';


		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-brio-helper-menus.php';


		/**
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'plugin-update-checker/plugin-update-checker.php';



		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-brio-helper-public.php';

		$this->loader = new Brio_Helper_Loader();

	}



	// /**
	//  *
	//  * @since    0.14
	//  * @access   private
	//  */
	// private function load_functions() {

	// 	$plugin_functions = new Brio_Helper_Functions();

	// }



	/**
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function load_widgets() {

		$plugin_widgets = new Brio_Helper_Widgets();

		$this->loader->add_action( 'widgets_init', $plugin_widgets, 'register' );

	}


	/**
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function add_shortcodes() {

		$plugin_shortcodes = new Brio_Helper_Shortcodes();

		add_shortcode( 'brio_half_left',  array('Brio_Helper_Shortcodes_Cols','left') );
		add_shortcode( 'brio_half_right', array('Brio_Helper_Shortcodes_Cols','right' ));

		add_shortcode( 'brio_col_1',  array('Brio_Helper_Shortcodes_Cols','col1' ));
		add_shortcode( 'brio_col_2',  array('Brio_Helper_Shortcodes_Cols','col2' ));
		add_shortcode( 'brio_col_3',  array('Brio_Helper_Shortcodes_Cols','col3') );

		add_shortcode( 'brio_social_icons', array('Brio_Helper_Shortcodes_Social','social_icons') );

	}


	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Brio_Helper_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Brio_Helper_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}


	/**
	 *
	 * Find out if there is an up to date version of this plugin bypassing WordPress's server.
	 *
	 * Uses Plugin Update Checker: https://github.com/YahnisElsts/plugin-update-checker
	 *
	 * @since    0.14
	 * @access   private
	 */
	public function check_for_update() {

		$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
			'http://brio.michellemay.co.uk/updates/brio.json',
			__FILE__,
			'brio-helper'
		);
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin 			= new Brio_Helper_Admin( $this->get_brio_helper(), $this->get_version() );
		$plugin_customizer 		= new Brio_Helper_Customizer( $this->get_brio_helper(), $this->get_version() );
		$plugin_menus 			= new Brio_Helper_Menus( $this->get_brio_helper(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_notices', $plugin_admin, 'enqueue_notices' );
		$this->loader->add_action( 'customize_register', $plugin_customizer, 'register' );
		$this->loader->add_action( 'customize_preview_init', $plugin_customizer, 'live_preview'  );
		$this->loader->add_action( 'customize_controls_print_styles', $plugin_customizer, 'brio_customizer_styles'  );
		$this->loader->add_action( 'admin_menu', $plugin_menus, 'add_menus' );

		add_action('wp_head', array('Brio_Helper_Menus_CSS','css_head'), 999);
		add_action('admin_init', array('Brio_Helper_Menus_CSS','settings'));

		add_action('admin_init', array('Brio_Helper_Menus_Hooks','settings'));
		add_action('admin_init', array('Brio_Helper_Menus_Links','settings'));

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    0.14
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public 			= new Brio_Helper_Public( $this->get_brio_helper(), $this->get_version() );
		$plugin_user_hooks		= new Brio_Helper_Menus_Hooks( $this->get_brio_helper(), $this->get_version() );


		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action('wp_head', $plugin_user_hooks, 'user_hook_head');
		$this->loader->add_action('brio_body_top', $plugin_user_hooks, 'user_hook_body_top');
		$this->loader->add_action('wp_footer', $plugin_user_hooks, 'user_hook_body_bottom');

		$this->loader->add_action('wp_footer', $plugin_user_hooks, 'user_hook_after_first_post');
		$this->loader->add_action('brio_entry_top', $plugin_user_hooks, 'user_hook_entry_top');
		$this->loader->add_action('brio_entry_bottom', $plugin_user_hooks, 'user_hook_entry_bottom');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.14
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since      0.14
	 * @return    string    The name of the plugin.
	 */
	public function get_brio_helper() {
		return $this->brio_helper;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since      0.14
	 * @return    Brio_Helper_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since      0.14
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}