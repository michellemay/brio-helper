<?php

/**
 * Loads the functions.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Loads the functions.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Functions {


	public function __construct(){

    $this->load_dependencies();

	}


  private function load_dependencies(){

   //require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-post-like.php';

  }

}


/**
 * Get an attachment ID given a URL.
 * 
 * @param string $url
 *
 * @return int Attachment ID on success, 0 on failure
 */
function brio_helper_get_attachment_id( $url ) {

	$attachment_id = 0;

	$dir = wp_upload_dir();

	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?

		$file = basename( $url );

		$query_args = array(
			'post_type'   => 'attachment',
			'post_status' => 'inherit',
			'fields'      => 'ids',
			'meta_query'  => array(
				array(
					'value'   => $file,
					'compare' => 'LIKE',
					'key'     => '_wp_attachment_metadata',
				),
			)
		);

		$query = new WP_Query( $query_args );

		if ( $query->have_posts() ) {

			foreach ( $query->posts as $post_id ) {

				$meta = wp_get_attachment_metadata( $post_id );

				$original_file       = basename( $meta['file'] );
				$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );

				if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
					$attachment_id = $post_id;
					break;
				}

			}

		}

	}

	return $attachment_id;
}

/*
 * Remove empty paragraphs input inside shortcode
*/

function shortcode_empty_paragraph_fix( $content ) {

    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array( 'brio_half_left', 'brio_half_right' );
    
    foreach ( $shortcodes as $shortcode ) {
        
        $array = array (
            '<p>[' . $shortcode => '[' .$shortcode,
            '<p>[/' . $shortcode => '[/' .$shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );

        $content = strtr( $content, $array );
    }

    return $content;
}

add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );


/*
 * Add Pinterest data to blog images
*/
if (!function_exists('brio_pinterest_rich_data')) {
	function brio_pinterest_rich_data($content) {
		
		$link = esc_url(get_the_permalink());
		$title = rawurldecode(get_the_title());
		$content = str_replace('<img','<img data-pin-description="'.$title.'" data-pin-url="'.$link.'"', $content);
		return $content;
		
	}
	add_filter('the_content','brio_pinterest_rich_data');
}

// check if this feature is enabled for this theme
// any enabled themes are passed in via array
function brio_theme_enabled($enabled_themes) {
	$this_theme = get_option('brio_theme');
	foreach($enabled_themes as $enabled_theme) {
		if ($this_theme == $enabled_theme) {
			return 1;
		} 
	}
	return 0;
}


include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-post-like.php';
include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-social-nav.php';
include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-feat-cat-posts.php';
include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-related-posts.php';
include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-profile.php';
include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-sharing.php';
// pin it in dev
//include plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/functions-brio-helper-pin-it.php';
