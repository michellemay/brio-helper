<?php

/**
 * Customizer functionality.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper_Customizer
 * @subpackage Brio_Helper/admin
 */

/**
 * Customizer functionality.
 *
 * @package    Brio_Helper_Customizer
 * @subpackage Brio_Helper/admin
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Customizer {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $brio_helper    The ID of this plugin.
	 */
	private $brio_helper;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.13
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.13
	 * @param      string    $brio_helper       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $brio_helper, $version ) {

		$this->brio_helper = $brio_helper;
		$this->version = $version;

	}

  function brio_customizer_styles()
  {
      ?>
      <style>
        #customize-theme-controls .customize-pane-child.open {
          z-index: 1000;
        }
      </style>
      <?php

  }

  public static function register($wp_customize)
  {

        $wp_customize->remove_section('colors');
        $wp_customize->remove_section('static_front_page');
        $wp_customize->remove_section('header_image');

        // SECTIONS ================================================================================


  }


  public static function live_preview()
  {
    //   wp_enqueue_script(
    //   'brio_helper_styles_preview', // Give the script a unique ID
    //   plugin_dir_url(__FILE__) . '/customize-preview.js', // Define the path to the JS file
    //   array(  'jquery', 'customize-preview' ), // Define dependencies
    //   '', // Define a version (optional)
    //   true // Specify whether to put in footer (leave this true)
    // );
  }

}
