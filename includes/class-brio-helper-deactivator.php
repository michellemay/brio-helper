<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.13
	 */
	public static function deactivate() {

	}

}
