<?php

/**
 * Fired during plugin activation
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.13
	 */
	public static function activate() {

	}

}
