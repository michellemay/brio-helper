<?php

/**
 * Loads the widgets.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Loads the widgets.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Widgets {


	public function __construct(){

    $this->load_dependencies();

	}


  private function load_dependencies(){

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/widget-brio-helper-popular-posts.php';

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/widget-brio-helper-about-me.php';

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/widget-brio-helper-promo-box.php';

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/widget-brio-helper-recent-posts.php';

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/widget-brio-helper-instagram.php';

  }


  /**
   * Register Widgets
   */
  function register() {

  	register_widget('brio_aboutme_widget');

    register_widget('brio_widget_promo_box');

    register_widget('brio_widget_popular_posts');

    register_widget('brio_widget_recent_posts');

    register_widget('brio_widget_instagram');

    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
  }


}
