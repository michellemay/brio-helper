<?php
/*
 * Add fields to profile page
*/

if ( ! defined( 'ABSPATH' ) ) exit;

if (!function_exists('brio_social_share')) { 
	function brio_social_share() {
	  
	    // Get current page URL 
	    $post_link = urlencode(get_permalink());

	    $post_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
		if ($post_thumb) {
			$img = esc_url($post_thumb['0']);
		} else {
			$img = brio_get_first_image();
		}

	    // Get current page title
	    $post_title = str_replace( ' ', '%20', get_the_title());

	    $twitterURL = 'https://twitter.com/intent/tweet?text='.$post_title.'&amp;url='.$post_link;
	    $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$post_link;
	    $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$post_link.'&amp;media='.$post_thumb[0].'&amp;description='.$post_title;
		$whatsappURL = 'whatsapp://send?text='.$post_title . ' ' . $post_link;
		$emailURL = 'mailto:?subject=Shared: '.$post_title.'&body='.$post_link;

    	$content = '<span class="brio-social-share">';
    	//$content .= '<p>';
    	
    	if(is_singular() && get_theme_mod('share_title')) {
	    	$content .= '<strong>'.get_theme_mod('share_title').'</strong> ';
	    }
    	if(get_theme_mod('share_on_twitter',1)) {
	        $content .= '<a href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter"></i></a> ';
	    }
    	if(get_theme_mod('share_on_facebook',1)) {
	        $content .= '<a href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook"></i></a> ';
	    }
    	if(get_theme_mod('share_on_pinterest',1)) {
	        $content .= '<a href="'.$pinterestURL.'" target="_blank"><i class="fa fa-pinterest"></i></a> ';
        }
		if(get_theme_mod('share_on_whatsapp',1)) {
	        $content .= '<a href="'.$whatsappURL.'" target="_blank"><i class="fa fa-whatsapp"></i></a> ';
        }
    	if(get_theme_mod('share_on_email',1)) {
	        $content .= '<a href="'.$emailURL.'" target="_blank"><i class="fa fa-envelope"></i></a>';
        }
        //$content .= '</p>';
        $content .= '</span>';

	   	//if(is_singular() && get_theme_mod('display_share_buttons') !== 3 || (is_home() || is_archive()) && get_theme_mod('display_share_buttons') == 2) {
	        echo $content;
	    //}
	}
	//add_action( 'brio_entry_bottom', 'brio_social_share', 1);
}

// customiser
if (!class_exists('share_buttons_customizer')) {
	class share_buttons_customizer {
		public static function register ( $wp_customize ) {
			
			$wp_customize->add_section( 'share_buttons_section', 
				array(
					'title' => __( 'Share Buttons', 'brio-helper' ),
					'description' => __( 'Control what share buttons are on your blog, and where', 'brio-helper' ),
					'capability' => 'edit_theme_options',
					'priority' => 25,
				) 
			);

		}
	}
	add_action( 'customize_register' , array( 'share_buttons_customizer' , 'register' ) );
}

function brio_helper_sharing_kirki_fields( $fields ) {
	
		$fields[] = array(
		    'settings' => 'display_share_buttons',
	        'type' => 'radio',
	        'label' => __('Enable / disable share buttons', 'brio-helper'),
	        'description' => __('Choose where to display your share buttons', 'brio-helper'),
	        'section' => 'share_buttons_section',
	        'choices' => array(
	            1 => __('Share buttons on single posts', 'brio-helper'),
	            2 => __('Share buttons on single posts and posts on homepage', 'brio-helper'),
	            3 => __('No share buttons', 'brio-helper'),
	        ),
		);

		$fields[] = array(
		    'settings' => 'share_title',
			'type' => 'text',
			'label' => __('Title (shows on single post only):'),
			'section' => 'share_buttons_section',
			'input_attrs' => array(
				'placeholder' => __('Share:', 'brio-helper'),
			),
		);

		$fields[] = array(
		    'settings' => 'share_on_twitter',
	        'type' => 'checkbox',
	        'label' => __( 'Share on Twitter', 'brio-helper' ),
	        'default' => 1,
	        'section' => 'share_buttons_section',
		);

		$fields[] = array(
		    'settings' => 'share_on_facebook',
	        'type' => 'checkbox',
	        'label' => __( 'Share on Facebook', 'brio-helper' ),
	        'default' => 1,
	        'section' => 'share_buttons_section',
		);

		$fields[] = array(
		    'settings' => 'share_on_pinterest',
	        'type' => 'checkbox',
	        'label' => __( 'Share on Pinterest', 'brio-helper' ),
	        'default' => 1,
	        'section' => 'share_buttons_section',
		);		

		$fields[] = array(
		    'settings' => 'share_on_whatsapp',
	        'type' => 'checkbox',
	        'label' => __( 'Share on WhatsApp', 'brio-helper' ),
	        'default' => 1,
	        'section' => 'share_buttons_section',
		);

		$fields[] = array(
		    'settings' => 'share_on_email',
	        'type' => 'checkbox',
	        'label' => __( 'Share via Email', 'brio-helper' ),
	        'default' => 1,
	        'section' => 'share_buttons_section',
		);		


	return $fields;
}
add_filter( 'kirki/fields', 'brio_helper_sharing_kirki_fields' );
