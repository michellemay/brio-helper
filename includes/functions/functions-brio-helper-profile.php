<?php
/*
 * Add fields to profile page
*/

if ( ! defined( 'ABSPATH' ) ) exit;

if (!function_exists('modify_author_fields')) { 

	function modify_author_fields($author_fields) {

		// Add new fields
		$author_fields['display-email'] = 'Contact Email (for user facing author biography)';
		$author_fields['author_twitter'] = 'Twitter URL';
		$author_fields['author_facebook'] = 'Facebook URL';
		$author_fields['author_instagram'] = 'Instagram URL';
		//$author_fields['author_gplus'] = 'Google+ URL';

		// Remove old fields

		return $author_fields;
	}
	add_filter('user_contactmethods', 'modify_author_fields');
}


// Function to add author card to bottom of posts

if (!function_exists('brio_author_card')) { 

	function brio_author_card( $content ) {

		$image_class = apply_filters( 'brio_author_image_class', 'col-3@s' );
		$text_class = apply_filters( 'brio_author_text_class', 'col-9@s' );

		if(is_single() && get_theme_mod('enable_author_profile', 1 )) { ?>
		<aside class="post-author-bio">
			<div class="o-grid">
				<div class="o-grid__item <?php echo $image_class; ?> author-image">
					<?php echo get_avatar( get_the_author_meta('email'), '200' ); ?>
				</div>
				<div class="o-grid__item <?php echo $text_class; ?> author-info">
					<h5 class="author-title">Written by <?php the_author_link(); ?></h5>
					<p class="author-description"><?php the_author_meta('description'); ?></p>
					<ul class="o-list-inline author-links">
						<?php 
							// $rss_url = get_the_author_meta( 'rss_url' );
							// if ( $rss_url && $rss_url != '' ) {
							// 	echo '<li class="rss"><a href="' . esc_url($rss_url) . '"></a></li>';
							// }
											
							$author_twitter = get_the_author_meta( 'author_twitter' );
							if ( $author_twitter && $author_twitter != '' ) {
								echo '<li class="o-list-inline__item"><a href="' . esc_url($author_twitter) . '"><i class="fa fa-twitter"></i></a></li>';
							}

							$author_instagram = get_the_author_meta( 'author_instagram' );
							if ( $author_instagram && $author_instagram != '' ) {
								echo '<li class="o-list-inline__item"><a href="' . esc_url($author_instagram) . '" rel="author"><i class="fa fa-instagram"></i></a></li>';
							}
							
							$author_facebook = get_the_author_meta( 'author_facebook' );
							if ( $author_facebook && $author_facebook != '' ) {
								echo '<li class="o-list-inline__item"><a href="' . esc_url($author_facebook) . '"><i class="fa fa-facebook"></i></a></li>';
							}
						?>
					</ul>
				</div>
			</div>
		</aside>
	<?php
		}
	}
	// Add our function to the post content filter 
	add_action( 'brio_entry_after', 'brio_author_card', 1 );

	// Allow HTML in author bio section 
	//remove_filter('pre_user_description', 'wp_filter_kses');
}


if (!class_exists('author_card_customizer')) {
	class author_card_customizer {
		
		public static function register ( $wp_customize ) {

			$wp_customize->add_section( 'brio_author_card', 
			    array(
			        'title' => __( 'Author Profile', 'brio-helper' ),
			        'description'=> __( 'Display the author\'s profile at the bottom of posts. Edit your profile under <a href="/wp-admin/profile.php">Users > Your Profile</a>.', 'brio-helper' ),
			        'capability' => 'edit_theme_options',
			        'priority' => 25,
			    ) 
			);
		}
	}
	add_action( 'customize_register' , array( 'author_card_customizer' , 'register' ) );
}

function brio_helper_profile_kirki_fields( $fields ) {

	$fields[] = array(
	    'settings' => 'enable_author_profile',
        'type' => 'checkbox',
        'label' => __( 'Enable this feature', 'brio-helper' ),
        'section' => 'brio_author_card',
	);

	$fields[] = array(
	    'settings' => 'author_profile_image_circle',
        'type' => 'checkbox',
        'label' => __( 'Make author avatar a circle', 'brio-helper' ),
        'section' => 'brio_author_card',
	);

	return $fields;
}
add_filter( 'kirki/fields', 'brio_helper_profile_kirki_fields' );

function brio_customized_styles() {

	$output = '';

	$avatar_shape = get_theme_mod('author_profile_image_circle');
    if ($avatar_shape) :
    	$output .= '.author-image .avatar{border-radius: 50%; -webkit-border-radius: 50%;}';
    endif;

    if ($output) {
        echo '<!-- User custom styles --><style>' . $output . '</style><!-- /User custom styles -->';
    }
   
}
add_action( 'wp_head', 'brio_customized_styles' );
