<?php 

if ( ! defined( 'ABSPATH' ) ) exit;

if (!function_exists('brio_pinterest_rich_data')) {
	function brio_pinterest_rich_data($content) {
		
		$link = esc_url(get_the_permalink());
		$title = rawurldecode(get_the_title());
		$content = str_replace('<img','<img data-pin-description="'.$title.'" data-pin-url="'.$link.'"', $content);
		return $content;
		
	}
	add_filter('the_content','brio_pinterest_rich_data');
}

// function Add_Pin_Button($content){

// 		wp_register_script("pinterest-js-script", "http://assets.pinterest.com/js/pinit.js", array(), '1.0.0', true);
// 		wp_enqueue_script("pinterest-js-script");
	
// 		global $post;

// 		// Get the post urls
// 		$posturl = urlencode(get_permalink());

// 		// Define a pattern to find all images inside the content
// 		$pattern = '/<img(.*?)src="(.*?).(bmp|gif|jpeg|jpg|png)" (.*?) width="(.*?)" height="(.*?)" \/>/i';
	  	
// 	  	// Replace the images with the following div and pin button
// 		$button_div = '
// 			<div class="pin-container">
// 				<img$1src="$2.$3" $4 width="$5" height="$6" />
// 				<div class="pin-it-button" style="top:-$6px;position:relative;">
// 				<a href="http://pinterest.com/pin/create/button/?url=
// 				'.$posturl.'
// 				&media=$2.$3
// 				&description='.urlencode(get_the_title()).'" count-layout="horizontal">Pin It</a></div>
// 			</div>';
// 		// Replace the images with a containing div with a pin button on the image
// 		$content = preg_replace( $pattern, $button_div, $content );

// 		return $content;
// 	}
// 	add_filter('the_content','Add_Pin_Button');

// if (!function_exists('brio_pinterest_hover')) {
// 	function brio_pinterest_hover() {
		
// 		if (!get_theme_mod('enable_pinterest_hover')) {
// 			return;
// 		}
		
// 		$margin = intval(get_theme_mod('brio_pinterest_hover_margin', 0));
// 		$position = esc_attr(get_theme_mod('brio_pinterest_hover_image_position', 'center'));
// 		// $pin_img = esc_url(get_theme_mod('brio_pinterest_hover_image_file', 'https://assets.pinterest.com/images/pidgets/pin_it_button.png'));
// 		$dec_prefix = '';
// 		if (get_theme_mod('brio_pinterest_hover_prefix_text')) {
// 			$dec_prefix = '%20'.esc_attr(get_theme_mod('brio_pinterest_hover_prefix_text'));
// 		}
		
// 	}
// 	add_action('wp_footer', 'brio_pinterest_hover', 999);
// }

// // customiser
// if (!class_exists('pinterest_hover_customizer')) {
// 	class pinterest_hover_customizer {
// 		public static function register ( $wp_customize ) {
			
// 			$wp_customize->add_section( 'pinterest_hover', 
// 				array(
// 					'title' => __( 'Pinterest Hover Button', 'brio-helper' ),
// 					'description'=> __( 'When you hover your mouse over an image in a post/page, a Pinterest "Pin it" button will appear.', 'brio-helper' ). ' You can download some of our custom Pinterest Hover Images on <a href="https://www.dropbox.com/sh/k8myt2vd8lgoz6a/AAD4w2WGe99Nr9wXpJl5T-TQa?dl=0" target="_blank">this page</a>',
// 					'capability' => 'edit_theme_options',
// 				) 
// 			);

// 		}
// 	}
// 	add_action( 'customize_register' , array( 'pinterest_hover_customizer' , 'register' ) );
// }

// function brio_helper_pin_it_kirki_fields( $fields ) {
	
// 		$fields[] = array(
// 		    'settings' => 'enable_pinterest_hover',
// 	        'type' => 'checkbox',
// 	        'label' => __('Enable / disable share buttons', 'brio-helper'),
// 	        'section' => 'pinterest_hover',
	
// 		);

// 	return $fields;
// }
// add_filter( 'kirki/fields', 'brio_helper_pin_it_kirki_fields' );

