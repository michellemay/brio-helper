<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if (!function_exists('brio_social_nav')) { 
	function brio_social_nav() {
		
		$navbar_icons = '';
		
		$links = get_option('brio_social_links');
		
		$twitter = $instagram = $facebook = $bloglovin = $pinterest = $youtube = $tumblr = $flickr = $snapchat = $email = $google_plus = $rss = '';
		
		if (!empty($links['twitter'])) {
			$twitter = esc_url($links['twitter']);
		}
		if (!empty($links['instagram'])) {
			$instagram = esc_url($links['instagram']);
		}
		if (!empty($links['facebook'])) {
			$facebook = esc_url($links['facebook']);
		}
		if (!empty($links['bloglovin'])) {
			$bloglovin = esc_url($links['bloglovin']);
		}
		if (!empty($links['pinterest'])) {
			$pinterest = esc_url($links['pinterest']);
		}
		if (!empty($links['snapchat'])) {
			$snapchat = esc_url($links['snapchat']);
		}
		if (!empty($links['youtube'])) {
			$youtube = esc_url($links['youtube']);
		}
		if (!empty($links['tumblr'])) {
			$tumblr = esc_url($links['tumblr']);
		}
		if (!empty($links['flickr'])) {
			$flickr = esc_url($links['flickr']);
		}
		if (!empty($links['google_plus'])) {
			$google_plus = esc_url($links['google_plus']);
		}
		if (!empty($links['email'])) {
			$email = sanitize_email($links['email']);
		}
		if (!empty($links['rss'])) {
			$rss = esc_url($links['rss']);
		}

		if($twitter && get_theme_mod('social_nav_twitter', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$twitter.'" target="_blank"><i class="fa fa-twitter"></i></a></li>';
		if($instagram && get_theme_mod('social_nav_instagram', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$instagram.'" target="_blank"><i class="fa fa-instagram"></i></a></li>';
		if($facebook && get_theme_mod('social_nav_facebook', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$facebook.'" target="_blank"><i class="fa fa-facebook"></i></a></li>';
		if($bloglovin && get_theme_mod('social_nav_bloglovin', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$bloglovin.'" target="_blank"><i class="fa fa-plus"></i></a></li>';
		if($pinterest && get_theme_mod('social_nav_pinterest', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$pinterest.'" target="_blank"><i class="fa fa-pinterest"></i></a></li>';
		if($snapchat && get_theme_mod('social_nav_snapchat', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$snapchat.'" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>';
		if($youtube && get_theme_mod('social_nav_youtube', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$youtube.'" target="_blank"><i class="fa fa-youtube-play"></i></a></li>';
		if($tumblr && get_theme_mod('social_nav_tumblr', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$tumblr.'" target="_blank"><i class="fa fa-tumblr"></i></a></li>';
		if($flickr && get_theme_mod('social_nav_flickr', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$flickr.'" target="_blank"><i class="fa fa-flickr"></i></a></li>';
		if($google_plus && get_theme_mod('social_nav_google_plus', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$google_plus.'" target="_blank"><i class="fa fa-google-plus"></i></a></li>';
		if($rss && get_theme_mod('social_nav_rss', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="'.$rss.'" target="_blank"><i class="fa fa-rss"></i></a></li>';
		if($email && get_theme_mod('social_nav_email', 1)) $navbar_icons .= '<li class="o-list-inline__item"><a href="mailto:'.$email.'" target="_blank"><i class="fa fa-envelope"></i></a></li>';

		if($navbar_icons) {
			echo "<ul class=\"o-list-inline social-links\">" .$navbar_icons. "</ul>";
		}
	}
}


// customiser
if (!class_exists('social_nav_icons_Customiser')) {
	class social_nav_icons_Customiser {
		public static function register ( $wp_customize ) {
			
			$wp_customize->add_section( 'social_nav_icons_section', 
				array(
					'title' 		=> __( 'Social Icons', 'brio-helper' ),
					'panel' 		=> 'brio_navigation',
					'description' 	=> __( 'Choose which social icons to add to your navigation below. You must have added your links in Brio Settings first.', 'brio-helper' ),
					'priority' 		=> 10,
				) 
			);
			
		}
	}
	add_action( 'customize_register' , array( 'social_nav_icons_Customiser' , 'register' ) );
}

function brio_helper_social_nav_kirki_fields( $fields ) {

		$fields[] = array(
		    'settings' => 'social_nav_twitter',
	        'type' => 'checkbox',
	        'label' => __( 'Twitter', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);		

		$fields[] = array(
		    'settings' => 'social_nav_instagram',
	        'type' => 'checkbox',
	        'label' => __( 'Instagram', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

		$fields[] = array(
		    'settings' => 'social_nav_facebook',
	        'type' => 'checkbox',
	        'label' => __( 'Facebook', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);		

		$fields[] = array(
		    'settings' => 'social_nav_email',
	        'type' => 'checkbox',
	        'label' => __( 'Email', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);		

		$fields[] = array(
		    'settings' => 'social_nav_bloglovin',
	        'type' => 'checkbox',
	        'label' => __( 'Bloglovin', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);		

		$fields[] = array(
		    'settings' => 'social_nav_pinterest',
	        'type' => 'checkbox',
	        'label' => __( 'Pinterest', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

		$fields[] = array(
		    'settings' => 'social_nav_tumblr',
	        'type' => 'checkbox',
	        'label' => __( 'Tumblr', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

		$fields[] = array(
		    'settings' => 'social_nav_snapchat',
	        'type' => 'checkbox',
	        'label' => __( 'Snapchat', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);		

		$fields[] = array(
		    'settings' => 'social_nav_youtube',
	        'type' => 'checkbox',
	        'label' => __( 'Youtube', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

		$fields[] = array(
		    'settings' => 'social_nav_flickr',
	        'type' => 'checkbox',
	        'label' => __( 'Flickr', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

		$fields[] = array(
		    'settings' => 'social_nav_google_plus',
	        'type' => 'checkbox',
	        'label' => __( 'Google Plus', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);					

		$fields[] = array(
		    'settings' => 'social_nav_rss',
	        'type' => 'checkbox',
	        'label' => __( 'RSS', 'brio-helper' ),
			'section' => 'social_nav_icons_section',
		);	

	return $fields;
}
add_filter( 'kirki/fields', 'brio_helper_social_nav_kirki_fields' );
