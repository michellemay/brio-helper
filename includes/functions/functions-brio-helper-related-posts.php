<?php
/*
 * Related posts
 * http://wordpress.stackexchange.com/questions/219584/how-to-tax-query-x-number-of-posts-related-by-tag-first-then-by-category-if-not?rq=1
*/

if ( ! defined( 'ABSPATH' ) ) exit;

function get_max_related_posts( $taxonomy_1 = 'post_tag', $taxonomy_2 = 'category', $total_posts = 4 )
{
    // First, make sure we are on a single page, if not, bail
    //if ( !is_single() )
        //return false;

    // Sanitize and vaidate our incoming data
    if ( 'post_tag' !== $taxonomy_1 ) {
        $taxonomy_1 = filter_var( $taxonomy_1, FILTER_SANITIZE_STRING );
        if ( !taxonomy_exists( $taxonomy_1 ) )
            return false;
    }

    if ( 'category' !== $taxonomy_2 ) {
        $taxonomy_2 = filter_var( $taxonomy_2, FILTER_SANITIZE_STRING );
        if ( !taxonomy_exists( $taxonomy_2 ) )
            return false;
    }

    if ( 4 !== $total_posts ) {
        $total_posts = filter_var( $total_posts, FILTER_VALIDATE_INT );
            if ( !$total_posts )
                return false;
    }

    // Everything checks out and is sanitized, lets get the current post
    $current_post = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );

    // Lets get the first taxonomy's terms belonging to the post
    $terms_1 = get_the_terms( $current_post, $taxonomy_1 );

    // Set a varaible to hold the post count from first query
    $count = 0;
    // Set a variable to hold the results from query 1
    $q_1   = [];


    $exclude_featured = get_term_by( 'slug', 'featured', 'post_tag' );
    $post_id = get_the_ID();

    // Make sure we have terms
    if ( $terms_1 ) {
        // Lets get the term ID's
        $term_1_ids = wp_list_pluck( $terms_1, 'term_id' );

        // Lets build the query to get related posts
        $args_1 = [
            //'post_type'      => $current_post->post_type,
            'post__not_in' => array($post_id),
            'posts_per_page' => $total_posts,
            'tag__not_in'    => $exclude_featured,
            'fields'         => 'ids',
            'tax_query'      => [
                [
                    'taxonomy'         => $taxonomy_1,
                    'terms'            => $term_1_ids,
                    'include_children' => false
                ]
            ],
        ];
        $q_1 = get_posts( $args_1 );
        // Count the total amount of posts
        $q_1_count = count( $q_1 );

        // Update our counter
        $count = $q_1_count;
    }

    // We will now run the second query if $count is less than $total_posts
    if ( $count < $total_posts ) {
        $terms_2 = get_the_terms( $current_post, $taxonomy_2 );
        // Make sure we have terms
        if ( $terms_2 ) {
            // Lets get the term ID's
            $term_2_ids = wp_list_pluck( $terms_2, 'term_id' );

            // Calculate the amount of post to get
            $diff = $total_posts - $count;

            // Create an array of post ID's to exclude
            // if ( $q_1 ) {
            //     $exclude = array_merge( [$current_post->ID], $q_1 );
            // } else {
            //     $exclude = [$current_post->ID];
            // }

            $args_2 = [
                //'post_type'      => $current_post->post_type,
                'post__not_in' => array($post_id),
                'posts_per_page' => $diff,
                'fields'         => 'ids',
                'tax_query'      => [
                    [
                        'taxonomy'         => $taxonomy_2,
                        'terms'            => $term_2_ids,
                        'include_children' => false
                    ]
                ],
            ];
            $q_2 = get_posts( $args_2 );

            if ( $q_2 ) {
                // Merge the two results into one array of ID's
                $q_1 = array_merge( $q_1, $q_2 );
            }
        }
    }

    // Make sure we have an array of ID's
    if ( !$q_1 )
        return false;

    // Run our last query, and output the results
    $final_args = [
        'ignore_sticky_posts' => 1,
        //'post_type'           => $current_post->post_type,
        'posts_per_page'      => count( $q_1 ),
        'post__in'            => $q_1,
        'order'               => 'ASC',
        'orderby'             => 'post__in',
        'suppress_filters'    => true,
        'no_found_rows'       => true
    ];
    $final_query = new WP_Query( $final_args );

    return $final_query;
}

if (!function_exists('brio_related_posts')) { 
    function brio_related_posts() {

        $related = get_theme_mod('enable_related_posts', '1');

        if($related == 3 ) {
            return;
        }

        if(is_singular() && $related !== 3 || is_home() && $related == 2) {

            $the_shape = absint(get_theme_mod('related_posts_image_shape', '1'));
            $img_shape = 'o-thumb--landscape';
        
            if ($the_shape == 2) {
                $img_shape = 'o-thumb--portrait';
            } elseif ($the_shape == 3) {
                $img_shape = 'o-thumb--square';
            }

            $related_title = strip_tags(get_theme_mod('related_posts_title'));
            if (empty($related_title)) {
                $related_title = __('You might also like', 'brio-helper');
            }

            $query = get_max_related_posts();
            if ( $query ) { ?>

            <aside class="related-posts">
                <div class="o-grid">
                    <div class="o-grid__item">
                        <h3 class="widget-title"><span><?php echo $related_title;?></span></h3>
                    </div>

                    <?php    
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        ?>

                        <div class="o-grid__item col-3@s col-6">

                            <a href="<?php the_permalink();?>" class="thumbnail-wrapper">
                                <div class="o-thumb <?php echo $img_shape;?>">
                                    <?php 
                                    if ( has_post_thumbnail() ) :  

                                        echo the_post_thumbnail( 'medium' );
                                    else:

                                        echo '<img src="'.brio_get_first_image().'"alt="" />';

                                    endif;
                                    ?>
                                </div>
                            </a>
                            <?php the_title( '<h5 class="h6 related-post__title"><a class="entry-title__link related-post__title-link" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' ); ?>
                        </div>

                    <?php }
                    wp_reset_postdata(); ?>

                </div>
            </aside>
            <?php 
            } 
        }
    }
    add_action('brio_entry_after', 'brio_related_posts', 2);
}

if (!class_exists('related_post_customizer')) {
    class related_post_customizer {
        public static function register ( $wp_customize ) {
            
            $wp_customize->add_section( 'related_posts_section', 
                array(
                    'title' => __( 'Related Posts', 'brio-helper' ),
                    'description'=> __( 'Related Posts are displayed from the same category.', 'brio-helper' ),
                    'capability' => 'edit_theme_options',
                    'priority' => 95,
                ) 
            );

        }
    }
    add_action( 'customize_register' , array( 'related_post_customizer' , 'register' ) );
}

function brio_helper_related_kirki_fields( $fields ) {

    $fields[] = array(
        'settings' => 'enable_related_posts',
        'type' => 'radio',
        'label' => __('Enable / disable related posts', 'brio-helper'),
        'description' => __("Choose where to display related posts. Homepage related posts will only work if you have a standard 1 column style layout.", 'brio-helper'),
        'section' => 'related_posts_section',
        'default' => '1',
        'choices' => array(
            '1' => __('Show on posts', 'brio-helper'),
            '2' => __('Show on posts and homepage', 'brio-helper'),
            '3' => __('Disable related posts', 'brio-helper'),
        ),
    );

    $fields[] = array(
        'settings' => 'related_posts_title',
        'type' => 'text',
        'label' => __( 'Title:', 'brio-helper' ),
        'section' => 'related_posts_section',
        'input_attrs' => array(
            'placeholder' => __('You might also like', 'brio-helper'),
        ),
    );

    $fields[] = array(
        'settings' => 'related_posts_image_shape',
        'type' => 'radio',
        'label' => __('Image shape', 'brio-helper'),
        'section' => 'related_posts_section',
        'default' => '1',
        'choices' => array(
            '1' => __('Landscape', 'brio-helper'),
            '2' => __('Portrait', 'brio-helper'),
            '3' => __('Square', 'brio-helper'),
        ),
    );
    return $fields;
}
add_filter( 'kirki/fields', 'brio_helper_related_kirki_fields' );

