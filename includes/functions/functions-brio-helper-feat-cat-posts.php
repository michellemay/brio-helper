<?php 
if (!function_exists('get_cat_post')) { 
	function get_cat_post($category) {

		$image_shape = get_theme_mod('featured_cat_post_image_shape', '3');
		$img_shape = '';
		if ($image_shape == 1) {
			$img_shape = 'o-thumb--landscape';
		} elseif ($image_shape == 2) {
			$img_shape = 'o-thumb--portrait';
		} elseif ($image_shape == 3) {
			$img_shape = 'o-thumb--square';
		}


		$cat_query = new WP_Query( array(
			'post_type'				=> 'post',
			'showposts'				=> 1,
			'ignore_sticky_posts'	=> true,
			'cat'					=> $category,
			)
		);

		if ( $cat_query->have_posts() ) :

		while ( $cat_query->have_posts() ): $cat_query->the_post();

			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
			if ($thumb) {
				$post_thumb = esc_url($thumb['0']);
			} else {
				$post_thumb = brio_get_first_image();
			}
			$post_cat = get_the_category();
			if ($post_cat) {
				$post_cat = $post_cat[0]->name;
			} else {
				$post_cat = '';
			}
			?>
			<div class="o-grid__item col-3@md col-6@s feat-cat-post">
				<h3 class="widget-title"><span><?php echo get_cat_name($category); ?></span></h3>
				<div class="feat-cat-post__image <?php echo $img_shape; ?>">
					<a href="<?php the_permalink();?>">
					<img src="o-thumb <?php echo $post_thumb; ?>" alt="<?php echo the_title(); ?>" data-pin-nopin="true" />
					</a>
				</div>
	
				<?php 
				if(get_option('brio_theme') != 'astoria') { ?>
					<div class="feat-cat-post__date c-meta">
						<?php echo get_the_date(); ?>
					</div>
				<?php } ?>

				<h4 class="feat-cat-post__title">				
					<a href="<?php the_permalink();?>" class="feat-cat-post__title-link"><?php the_title(); ?></a>
				</h4>

				<?php 
				if(get_option('brio_theme') == 'astoria') { ?>
					<div class="feat-cat-post__date c-meta">
						<?php echo get_the_date(); ?>
					</div>
				<?php } ?>

			</div>

		<?php
		endwhile; 
		endif;
		wp_reset_query();
	}
}

if (!function_exists('brio_featured_cat_post')) { 
	function brio_featured_cat_post($category) {

		if (!is_home() && !is_front_page() || !get_theme_mod('show_featured_cat_posts')) {
			return;
		}

		$post_cat_1 = get_theme_mod('feat_cat_1');
		$post_cat_2 = get_theme_mod('feat_cat_2');
		$post_cat_3 = get_theme_mod('feat_cat_3');
		$post_cat_4 = get_theme_mod('feat_cat_4');

		$mobile = get_theme_mod('show_featured_cat_posts_mobile', 1);

		?>
		<section class="featured-cat-posts" <?php if($mobile) { echo 'style="display:block;"'; }; ?>">
			<div class="o-wrapper">

				<div class="o-grid">
				
				<?php if($post_cat_1) { get_cat_post($post_cat_1); } ?>
				<?php get_cat_post($post_cat_2); ?>
				<?php get_cat_post($post_cat_3); ?>
				<?php get_cat_post($post_cat_4); ?>
				
				</div>
			</div>
		</section>
	<?php
	}
	add_action('brio_content_before','brio_featured_cat_post',5);
}



if (!class_exists('featured_cat_post_customizer')) {
	class featured_cat_post_customizer {
		
		public static function register ( $wp_customize ) {

			$wp_customize->add_section( 'featured_cat_posts', 
			    array(
			        'title' => __( 'Featured Category Posts', 'brio-helper' ),
			        'description'=> __( 'Display the most recent post from 4 of your blog\'s categories', 'brio-helper' ),
			        'capability' => 'edit_theme_options',
			        'priority' => 10,
			        'panel' => 'homepage',
			    ) 
			);
		}
	}
	add_action( 'customize_register' , array( 'featured_cat_post_customizer' , 'register' ) );
}

function brio_helper_cats_kirki_fields( $fields ) {

	$fields[] = array(
	    'settings' => 'show_featured_cat_posts',
        'type' => 'checkbox',
        'label' => __( 'Enable this feature', 'brio-helper' ),
        'section' => 'featured_cat_posts',
	);

	$fields[] = array(
	    'settings' => 'show_featured_cat_posts_mobile',
        'type' => 'checkbox',
        'label' => __( 'Show on mobile', 'brio-helper' ),
        'description' => __('Select this box to show this feature on mobile. We recommend keeping this off - this feature looks great on desktop, but on mobile can be a distraction to your latests posts.', 'brio-helper'),
        'section' => 'featured_cat_posts',
	);

    $fields[] = array(
		'settings'    => 'featured_cat_post_image_shape',
		'type'        => 'radio',
		'label'       => __( 'Image shape', 'brio-helper' ),
		'section'     => 'featured_cat_posts',
		'default'     => '3',
		'choices'     => array(
			'1' => __('Landscape', 'brio-helper'),
			'2' => __('Portrait', 'brio-helper'),
			'3' => __('Square', 'brio-helper'),
		),
    );   

 //    if(!empty (array_keys(Kirki_Helper::get_terms( 'category' )))) {
	//     $defaults = array_keys(Kirki_Helper::get_terms( 'category' ));
	// 	$default = min($defaults);
	// } else {
	// 	$default = 'Error: Your blog has no posts';
	// }


    $fields[] = array(
		'settings'    => 'feat_cat_1',
		'type'        => 'select',
		'label'       => __( 'Choose a category', 'brio-helper' ),
		'section'     => 'featured_cat_posts',
		'default'     => '', 
 		'placeholder' => esc_attr__( 'Select a category', 'brio-celeste' ),
		'choices'     => Kirki_Helper::get_terms( 'category' ),
    );

    $fields[] = array(
		'settings'    => 'feat_cat_2',
		'type'        => 'select',
		'label'       => __( 'Choose a category', 'brio-helper' ),
		'section'     => 'featured_cat_posts',
		'default'     => '', 
 		'placeholder' => esc_attr__( 'Select a category', 'brio-celeste' ),
		'choices'     => Kirki_Helper::get_terms( 'category' ),
    );

    $fields[] = array(
		'settings'    => 'feat_cat_3',
		'type'        => 'select',
		'label'       => __( 'Choose a category', 'brio-helper' ),
		'section'     => 'featured_cat_posts',
		'default'     => '', 
 		'placeholder' => esc_attr__( 'Select a category', 'brio-celeste' ),
		'choices'     => Kirki_Helper::get_terms( 'category' ),
    );

    $fields[] = array(
		'settings'    => 'feat_cat_4',
		'type'        => 'select',
		'label'       => __( 'Choose a category', 'brio-helper' ),
		'section'     => 'featured_cat_posts',
		'default'     => '', 
 		'placeholder' => esc_attr__( 'Select a category', 'brio-celeste' ),
		'choices'     => Kirki_Helper::get_terms( 'category' ),
    );       


	return $fields;
}
add_filter( 'kirki/fields', 'brio_helper_cats_kirki_fields' );
