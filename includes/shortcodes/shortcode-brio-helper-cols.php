<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Brio_Helper_Shortcodes_Cols{

	// [brio_half_left] [/brio_half_left]
	static function left( $atts, $content = null ) {
		return '<div class="clearfix"></div><div class="brio-half col-6@sm bh-left">'.do_shortcode($content).'</div>';
	}

	// [brio_half_right] [/brio_half_right]
	static function right( $atts, $content = null ) {
		return '<div class="brio-half col-6@sm bh-right">'.do_shortcode($content).'</div><div class="clearfix"></div>';
	}

	// // [brio_col_1] [/brio_col_1]
	// static function col1( $atts, $content = null ) {
	// 	return '<div class="brio-col-1">'.do_shortcode($content).'</div>';
	// }

	// // [brio_col_2] [/brio_col_2]
	// static function col2( $atts, $content = null ) {
	// 	return '<div class="brio-col-2">'.do_shortcode($content).'</div>';
	// }

	// // [brio_col_3] [/brio_col_3]
	// static function col3( $atts, $content = null ) {
	// 	return '<div class="brio-col-3">'.do_shortcode($content).'</div><div class="clearfix"></div>';
	// }

}
