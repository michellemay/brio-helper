<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Brio_Helper_Shortcodes_Social{

	// [brio_social_icons size="15px" color=""]
	static function social_icons( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'size' => '',
			'color' => ''
		), $atts ) );

		$links = get_option('brio_social_links');

		$output = '<div class="social bh-social-icons">';
		// if ($size || $color) {
		// 	$output .= ' style="font-size:' .$size. ';color:' .$color. ';"';
		// }
		// $output .= '>';

		if ($size || $color) {
			$output .= '<style scoped>.bh-social-icons a {font-size:'.$size.';color:'.$color.';}</style>';
		}

			if (!empty($links['twitter'])) {
				$output .= '<a href="'.esc_url($links['twitter']).'" target="_blank"><i class="fa fa-twitter"></i></a>';
			}
			if (!empty($links['instagram'])) {
				$output .= '<a href="'.esc_url($links['instagram']).'" target="_blank"><i class="fa fa-instagram"></i></a>';
			}
			if (!empty($links['facebook'])) {
				$output .= '<a href="'.esc_url($links['facebook']).'" target="_blank"><i class="fa fa-facebook"></i></a>';
			}
			if (!empty($links['bloglovin'])) {
				$output .= '<a href="'.esc_url($links['bloglovin']).'" target="_blank"><i class="fa fa-plus"></i></a>';
			}
			if (!empty($links['pinterest'])) {
				$output .= '<a href="'.esc_url($links['pinterest']).'" target="_blank"><i class="fa fa-pinterest"></i></a>';
			}
			if (!empty($links['youtube'])) {
				$output .= '<a href="'.esc_url($links['youtube']).'" target="_blank"><i class="fa fa-youtube-play"></i></a>';
			}
			if (!empty($links['tumblr'])) {
				$output .= '<a href="'.esc_url($links['tumblr']).'" target="_blank"><i class="fa fa-tumblr"></i></a>';
			}
			if (!empty($links['flickr'])) {
				$output .= '<a href="'.esc_url($links['flickr']).'" target="_blank"><i class="fa fa-flickr"></i></a>';
			}
			if (!empty($links['email'])) {
				$output .= '<a href="mailto:'.sanitize_email($links['email']).'"><i class="fa fa-envelope"></i></a>';
			}

		$output .= '</div>';

		return $output;
	}



}
