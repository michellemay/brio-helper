<?php

/**
 * Loads the admin menus.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Loads the admin menus.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Menus {


	public function __construct(){

    $this->load_dependencies();

	}


  private function load_dependencies(){

	 require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/menus/menus-brio-helper-welcome.php';

	 require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/menus/menus-brio-helper-shortcodes.php';

	 require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/menus/menus-brio-helper-css.php';

	 require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/menus/menus-brio-helper-hooks.php';

	 require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/menus/menus-brio-helper-links.php';

  }


  /**
   * Register Widgets
   */
	 function add_menus() {

 		global $submenu;
 		add_menu_page( 'brio-helper', 'Brio Settings', 'delete_others_pages', 'brio-helper', array('Brio_Helper_Menus_Welcome','welcome_options_page'), 'dashicons-welcome-widgets-menus' );
 		add_submenu_page( 'brio-helper', 'Shortcodes', 'Shortcodes', 'delete_others_pages', 'brio-shortcodes', array('Brio_Helper_Menus_Shortcodes','shortcodes_options_page') );
 		add_submenu_page( 'brio-helper', __('Social Links', 'brio-helper'), __('Social Links', 'brio-helper'), 'delete_others_pages', 'brio-social-links', array('Brio_Helper_Menus_Links','social_links_options_page') );
 		add_submenu_page( 'brio-helper', __('Custom CSS', 'brio-helper'), __('Custom CSS', 'brio-helper'), 'manage_options', 'brio-css', array('Brio_Helper_Menus_CSS','css_options_page') );
 		add_submenu_page( 'brio-helper', __('Theme').' Hooks', __('Theme').' Hooks', 'manage_options', 'brio-hooks', array('Brio_Helper_Menus_Hooks','hooks_options_page' ));

 		$submenu['brio-helper'][0][0] = __('Welcome', 'brio-helper'); // http://wordpress.stackexchange.com/questions/98226/admin-menus-name-menu-different-from-first-submenu
 	}

}
