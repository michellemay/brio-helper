<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'brio_widget_instagam' ) ) {
	
	class brio_widget_instagram extends WP_Widget {

		public function __construct() {
			$widget_ops = array('classname' => 'brio-widget-instagram', 'description' => __('Displays your latest Instagram photos.', 'brio-helper'), 'customize_selective_refresh' => true );
			parent::__construct('brio_widget_instagram',  __('Brio Helper: Instagram', 'brio-helper'), $widget_ops);
		}

		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 
				'title' => __( 'Instagram', 'brio-helper' ), 
				'username' => '',
				'col_number' => 'col-1', 
				'padding' => '', 
				'link' => __( 'Follow me on Instagram', 'brio-helper' ), 
				'link_header' => 0,
				'mobile' => '',
				'photo_number' => 4, 
				'target' => '_self' ) 
			);
			$title = $instance['title'];
			$username = $instance['username'];
			$photo_number = absint( $instance['photo_number'] );
			$col_number = $instance['col_number'];  
			$padding =  absint( $instance['padding'] ); 
			$mobile = $instance['mobile']; 
			$target = $instance['target'];
			$link = $instance['link'];
			$link_header = $instance['link_header'];
			?>

			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'brio-helper' ); ?>: 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( '@username or #tag', 'brio-helper' ); ?>: 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" type="text" value="<?php echo esc_attr( $username ); ?>" /></label>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'photo_number' ) ); ?>"><?php esc_html_e( 'Number of Photos (max 20)', 'brio-helper' ); ?>: 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'photo_number' ) ); ?>" type="text" value="<?php echo esc_attr( $photo_number ); ?>" /></label>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'padding' ) ); ?>"><?php esc_html_e( 'Padding around images (in pixels)', 'brio-helper' ); ?>:<br> 
				<input class="widefat" style="max-width: 50px;" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'padding' ) ); ?>" type="number" value="<?php echo esc_attr( $padding ); ?>" /> px</label>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'col_number' ) ); ?>"><?php esc_html_e( 'Number of Columns', 'brio-helper' ); ?>:</label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'col_number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'col_number' ) ); ?>" class="widefat">
					<option value="col-1" <?php selected( 'col-1', $instance['col_number'] ) ?>><?php esc_html_e( '1', 'brio-helper' ); ?></option>
					<option value="col-2" <?php selected( 'col-2', $instance['col_number'] ) ?>><?php esc_html_e( '2', 'brio-helper' ); ?></option>
					<option value="col-3" <?php selected( 'col-3', $instance['col_number'] ) ?>><?php esc_html_e( '3', 'brio-helper' ); ?></option>
					<option value="col-4" <?php selected( 'col-4', $instance['col_number'] ) ?>><?php esc_html_e( '4', 'brio-helper' ); ?></option>
					<option value="col-5" <?php selected( 'col-5', $instance['col_number'] ) ?>><?php esc_html_e( '5', 'brio-helper' ); ?></option>
					<option value="col-6" <?php selected( 'col-6', $instance['col_number'] ) ?>><?php esc_html_e( '6', 'brio-helper' ); ?></option>
					<option value="col-7" <?php selected( 'col-7', $instance['col_number'] ) ?>><?php esc_html_e( '7', 'brio-helper' ); ?></option>
					<option value="col-8" <?php selected( 'col-8', $instance['col_number'] ) ?>><?php esc_html_e( '8', 'brio-helper' ); ?></option>
					<option value="col-9" <?php selected( 'col-9', $instance['col_number'] ) ?>><?php esc_html_e( '9', 'brio-helper' ); ?></option>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('mobile'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('mobile'); ?>" name="<?php echo $this->get_field_name('mobile'); ?>" <?php if (isset($instance['mobile'])) { checked((bool) $instance['mobile'], true); } ?> /><?php _e('Disable mobile layout (recommended if widget is in sidebar)', 'brio-helper'); ?>
				</label>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php esc_html_e( 'Open links in', 'brio-helper' ); ?>:</label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" class="widefat">
					<option value="_self" <?php selected( '_self', $target ) ?>><?php esc_html_e( 'Current window (_self)', 'brio-helper' ); ?></option>
					<option value="_blank" <?php selected( '_blank', $target ) ?>><?php esc_html_e( 'New window (_blank)', 'brio-helper' ); ?></option>
				</select>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Follow link text (keep blank for no link)', 'brio-helper' ); ?>: <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" /></label></p>
			<p>
				<label for="<?php echo $this->get_field_id('link_header'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('link_header'); ?>" name="<?php echo $this->get_field_name('link_header'); ?>" <?php if (isset($instance['link_header'])) { checked((bool) $instance['link_header'], true); } ?> /><?php _e('Move link text into header', 'brio-helper'); ?>
				</label>
			</p>			
			<?php

		}

		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['username'] = trim( strip_tags( $new_instance['username'] ) );
			$instance['photo_number'] = ! absint( $new_instance['photo_number'] ) ? 20 : $new_instance['photo_number'];
			$instance['col_number'] = strip_tags($new_instance['col_number']);
			$instance['padding'] = absint($new_instance['padding']);
			$instance['mobile'] = strip_tags($new_instance['mobile']);
			$instance['target'] = ( ( $new_instance['target'] == '_self' || $new_instance['target'] == '_blank' ) ? $new_instance['target'] : '_self' );
			$instance['link'] = strip_tags( $new_instance['link'] );
			$instance['link_header'] = strip_tags($new_instance['link_header']);
			return $instance;
		}

		// based on https://gist.github.com/cosmocatalano/4544576
		function scrape_instagram( $username ) {

			$username = trim( strtolower( $username ) );

			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url              = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );
					$transient_prefix = 'h';
					break;

				default:
					$url              = 'https://instagram.com/' . str_replace( '@', '', $username );
					$transient_prefix = 'u';
					break;
			}

			if ( false === ( $instagram = get_transient( 'insta-a10-' . $transient_prefix . '-' . sanitize_title_with_dashes( $username ) ) ) ) {

				$remote = wp_remote_get( $url );

				if ( is_wp_error( $remote ) ) {
					return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'wp-instagram-widget' ) );
				}

				if ( 200 !== wp_remote_retrieve_response_code( $remote ) ) {
					return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'wp-instagram-widget' ) );
				}

				$shards      = explode( 'window._sharedData = ', $remote['body'] );
				$insta_json  = explode( ';</script>', $shards[1] );
				$insta_array = json_decode( $insta_json[0], true );

				if ( ! $insta_array ) {
					return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
				}

				if ( isset( $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] ) ) {
					$images = $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
				} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'] ) ) {
					$images = $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
				} else {
					return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
				}

				if ( ! is_array( $images ) ) {
					return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
				}

				$instagram = array();

				foreach ( $images as $image ) {
					if ( true === $image['node']['is_video'] ) {
						$type = 'video';
					} else {
						$type = 'image';
					}

					$caption = __( 'Instagram Image', 'wp-instagram-widget' );
					if ( ! empty( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ) {
						$caption = wp_kses( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'], array() );
					}

					$instagram[] = array(
						'description' => $caption,
						'link'        => trailingslashit( '//instagram.com/p/' . $image['node']['shortcode'] ),
						'time'        => $image['node']['taken_at_timestamp'],
						'comments'    => $image['node']['edge_media_to_comment']['count'],
						'likes'       => $image['node']['edge_liked_by']['count'],
						'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ),
						'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ),
						'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][4]['src'] ),
						'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
						'type'        => $type,
					);
				} // End foreach().

				// do not set an empty transient - should help catch private or empty accounts.
				if ( ! empty( $instagram ) ) {
					$instagram = base64_encode( serialize( $instagram ) );
					set_transient( 'insta-a10-' . $transient_prefix . '-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
				}
			}

			if ( ! empty( $instagram ) ) {

				return unserialize( base64_decode( $instagram ) );

			} else {

				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'wp-instagram-widget' ) );

			}
		}

		function images_only( $media_item ) {

			if ( 'image' === $media_item['type'] ) {
				return true;
			}

			return false;
		}

		public function widget( $args, $instance ) {

			$title = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
			$username = empty( $instance['username'] ) ? '' : $instance['username'];
			$limit = empty( $instance['photo_number'] ) ? 20 : $instance['photo_number'];
			$col_number = empty( $instance['col_number'] ) ? '' : $instance['col_number'];
			$padding = empty( $instance['padding'] ) ? 0 : $instance['padding'];
			$mobile = empty( $instance['mobile'] ) ? '' : $instance['mobile'];
			$target = empty( $instance['target'] ) ? '_self' : $instance['target'];
			$link = empty( $instance['link'] ) ? '' : $instance['link'];
			$link_header = empty( $instance['link_header'] ) ? '' : $instance['link_header'];

			$linkclass = apply_filters( 'wpiw_link_class', 'clear' );
			$linkaclass = apply_filters( 'wpiw_linka_class', '' );
			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url = '//instagram.com/explore/tags/' . str_replace( '#', '', $username );
					break;
				default:
					$url = '//instagram.com/' . str_replace( '@', '', $username );
					break;
			}

			echo $args['before_widget'];

			echo '<div class="c-feed-wrap">';

			if ( ! empty( $title ) ) { 
				echo $args['before_title'];
				echo wp_kses_post( $title ); 
				if ( '' !== $link && $link_header ) {
					echo ' <a href="' . trailingslashit( esc_url( $url ) ) . '" rel="me" target="' . esc_attr( $target ) . '" class="' . esc_attr( $linkaclass ) .'">'. wp_kses_post( $link ) . '</a>';
				}
				echo $args['after_title']; 
			};

			if ( '' !== $username ) {

				$media_array = $this->scrape_instagram( $username );

				if ( is_wp_error( $media_array ) ) {

					echo wp_kses_post( $media_array->get_error_message() );

				} else {
					// filter for images only?

					if ( $images_only = apply_filters( 'wpiw_images_only', false ) ) {
						$media_array = array_filter( $media_array, array( $this, 'images_only' ) );
					}

					// slice list down to required limit.
					$media_array = array_slice( $media_array, 0, $limit );

					// filters for custom classes.
					$ulclass = apply_filters( 'wpiw_list_class', 'instagram-pics bhig-' . $col_number );
					$liclass = 'bh-' . $col_number;
					
					?>

					<ul class="bhig <?php if(!$mobile){ echo 'mobile'; } ?>" <?php if($padding) { echo 'style="margin-left:-'.$padding.'px"'; } ?>>

					<?php
					foreach ( $media_array as $item ) {?>				
						<li class="bhig__item <?php echo $liclass; ?>" <?php if($padding) { echo 'style="padding-left:'.$padding.'px;padding-bottom:'.$padding.'px"'; } ?>>

						<?php echo '<a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"  class=""><img src="'. esc_url( $item['large'] ) .'"  alt="Instagram image" data-pin-nopin="true" /></a></li>';
					}
					?>
					</ul>
					<?php
				}
			}

			echo '</div>';

			
			if ( '' !== $link && !$link_header ) {
				?><span class="bhig-follow"><a href="<?php echo trailingslashit( esc_url( $url ) ); ?>" rel="me" target="<?php echo esc_attr( $target ); ?>" class="<?php echo esc_attr( $linkaclass ); ?>"><?php echo wp_kses_post( $link ); ?></a></span><?php
			}


			echo $args['after_widget'];
		}

	}
}