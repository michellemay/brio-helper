<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'brio_widget_popular_posts' ) ) {
	class brio_widget_popular_posts extends WP_Widget {

		public function __construct() {
			$widget_ops = array('classname' => 'bh-widget-widget-posts', 'description' => __('Displays your most popular posts.', 'brio-helper') );
			parent::__construct('brio_widget_popular_posts',  __('Brio Helper: Popular Posts', 'brio-helper'), $widget_ops);
		}

		public function form($instance) {
			$instance = wp_parse_args( (array) $instance, array( 
				'title' => __( '', 'brio-helper' ), 
				'date_range_posts' => '',
				'number_posts' => 3, 
				'col_number' => 1, 
				'display_style' => 1,
				'image_shape' => 1,
				'comments' => 0,
			));

			$title = $instance['title'];
			$date_range_posts = $instance['date_range_posts'];
			$number_posts = absint( $instance['number_posts'] );
			$col_number = absint( $instance['col_number'] );  
			$display_style =  absint( $instance['display_style'] ); 
			$image_shape =  absint( $instance['image_shape'] ); 
			$comments = $instance['comments']; 
			?>

			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>" type="text"
				value="<?php echo esc_attr($title); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('date_range_posts'); ?>"><?php _e('Date range for posts:', 'brio-helper'); ?></label>
				<select id="<?php echo $this->get_field_id( 'date_range_posts' ); ?>" name="<?php echo $this->get_field_name( 'date_range_posts' ); ?>">
					<option <?php if ( '1 week ago' == $date_range_posts ) echo 'selected="selected"'; ?> value="1 week ago"><?php _e('1 Week', 'brio-helper') ?></option>
					<option <?php if ( '1 month ago' == $date_range_posts ) echo 'selected="selected"'; ?> value="1 month ago"><?php _e('1 Month', 'brio-helper') ?></option>
					<option <?php if ( '3 months ago' == $date_range_posts ) echo 'selected="selected"'; ?> value="3 months ago"><?php _e('3 Months', 'brio-helper') ?></option>
					<option <?php if ( '6 months ago' == $date_range_posts ) echo 'selected="selected"'; ?> value="6 months ago"><?php _e('6 Months', 'brio-helper') ?></option>
					<option <?php if ( '1 year ago' == $date_range_posts ) echo 'selected="selected"'; ?> value="1 year ago"><?php _e('1 Year', 'brio-helper') ?></option>
					<option <?php if ( '' == $date_range_posts ) echo 'selected="selected"'; ?> value=""><?php _e('All Time', 'brio-helper') ?></option>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('number_posts'); ?>"><?php _e('Number of posts to show:', 'brio-helper'); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'number_posts' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number_posts' ) ); ?>">
					<option value="1" <?php selected( '1', $instance['number_posts'] ) ?>><?php esc_html_e( '1', 'brio-helper' ); ?></option>
					<option value="2" <?php selected( '2', $instance['number_posts'] ) ?>><?php esc_html_e( '2', 'brio-helper' ); ?></option>
					<option value="3" <?php selected( '3', $instance['number_posts'] ) ?>><?php esc_html_e( '3', 'brio-helper' ); ?></option>
					<option value="4" <?php selected( '4', $instance['number_posts'] ) ?>><?php esc_html_e( '4', 'brio-helper' ); ?></option>
					<option value="5" <?php selected( '5', $instance['number_posts'] ) ?>><?php esc_html_e( '5', 'brio-helper' ); ?></option>
					<option value="6" <?php selected( '6', $instance['number_posts'] ) ?>><?php esc_html_e( '6', 'brio-helper' ); ?></option>
				</select>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'col_number' ) ); ?>"><?php esc_html_e( 'Number of Columns', 'brio-helper' ); ?>:</label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'col_number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'col_number' ) ); ?>" class="widefat">
					<option value="1" <?php selected( '1', $instance['col_number'] ) ?>><?php esc_html_e( '1', 'brio-helper' ); ?></option>
					<option value="2" <?php selected( '2', $instance['col_number'] ) ?>><?php esc_html_e( '2', 'brio-helper' ); ?></option>
					<option value="3" <?php selected( '3', $instance['col_number'] ) ?>><?php esc_html_e( '3', 'brio-helper' ); ?></option>
					<option value="4" <?php selected( '4', $instance['col_number'] ) ?>><?php esc_html_e( '4', 'brio-helper' ); ?></option>
					<option value="5" <?php selected( '5', $instance['col_number'] ) ?>><?php esc_html_e( '5', 'brio-helper' ); ?></option>
					<option value="6" <?php selected( '6', $instance['col_number'] ) ?>><?php esc_html_e( '6', 'brio-helper' ); ?></option>
				</select>
			</p>	
			<p>
				<legend><h3><?php _e('Select a layout:', ''); ?></h3></legend>
				<input type="radio" id="<?php echo $this->get_field_id('display_style_1'); ?>" name="<?php echo ($this->get_field_name( 'display_style' )) ?>" value="1" <?php checked( $display_style == 1, true) ?>>
				<label for="<?php echo $this->get_field_id('display_style_1'); ?>"><?php _e('Small image floated left with text');?></label>
				<br><br>
				<input type="radio" id="<?php echo $this->get_field_id('display_style_2'); ?>" name="<?php echo ($this->get_field_name( 'display_style' )) ?>" value="2" <?php checked( $display_style == 2, true) ?>>
				<label for="<?php echo $this->get_field_id('display_style_2'); ?>"><?php _e('Text beneath image');?></label>
				<br><br>
				<input type="radio" id="<?php echo $this->get_field_id('display_style_3'); ?>" name="<?php echo ($this->get_field_name( 'display_style' )) ?>" value="3" <?php checked( $display_style == 3, true) ?>>
				<label for="<?php echo $this->get_field_id('display_style_3'); ?>"><?php _e('Text on image');?></label>
			</p>
			<p>
				<legend><h3><?php _e('Image shape:', ''); ?></h3></legend>
				<input type="radio" id="<?php echo $this->get_field_id('image_shape_1'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="1" <?php checked( $image_shape == 1, true) ?>>
				<label for="<?php echo $this->get_field_id('image_shape_1'); ?>"><?php _e('Landscape');?></label>
				<br><br>
				<input type="radio" id="<?php echo $this->get_field_id('image_shape_2'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="2" <?php checked( $image_shape == 2, true) ?>>
				<label for="<?php echo $this->get_field_id('image_shape_2'); ?>"><?php _e('Portrait');?></label>
				<br><br>
				<input type="radio" id="<?php echo $this->get_field_id('image_shape_3'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="3" <?php checked( $image_shape == 3, true) ?>>
				<label for="<?php echo $this->get_field_id('image_shape_3'); ?>"><?php _e('Square');?></label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('comments'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('comments'); ?>" name="<?php echo $this->get_field_name('comments'); ?>" <?php if (isset($instance['comments'])) { checked((bool) $instance['comments'], true); } ?> /><?php _e('Don\'t show comment count', 'brio-helper'); ?>
				</label>
			</p>
		<?php
		}

		function update($new_instance, $old_instance) {
			delete_transient('brio_widget_popular_posts'); // delete transient on widget save
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['date_range_posts'] =  strip_tags($new_instance['date_range_posts']);
			$instance['number_posts'] = absint($new_instance['number_posts']);
			$instance['col_number'] = absint($new_instance['col_number']);
			$instance['display_style'] = ( isset( $new_instance['display_style'] ) && $new_instance['display_style'] > 0 && $new_instance['display_style'] < 4 ) ? (int) $new_instance['display_style'] : 0;
			$instance['image_shape'] = ( isset( $new_instance['image_shape'] ) && $new_instance['image_shape'] > 0 && $new_instance['image_shape'] < 4 ) ? (int) $new_instance['image_shape'] : 0;
			$instance['comments'] = strip_tags($new_instance['comments']);
			return $instance;
		}

		public function widget($args, $instance) {
			extract($args, EXTR_SKIP);

			echo $before_widget;
			if (isset($instance['title'])) {
				$title = strip_tags($instance['title']);
			}
			if (isset($instance['number_posts'])) {
				$number_posts = absint($instance['number_posts']);
			} else {
				$number_posts = 3;
			}

			if (isset($instance['col_number'])) {
				$col_number = absint($instance['col_number']);
			} else {
				$col_number = 1;
			}

			//$col_number = empty( $instance['col_number'] ) ? '' : $instance['col_number'];
		
			if (isset($instance['date_range_posts'])) {
				$date_range_posts = strip_tags($instance['date_range_posts']);
			} else {
				$date_range_posts = '';
			}
			if (isset($instance['display_style'])) { 
				$display_style = $instance['display_style'];
			} else {
				$display_style = 1;
			}
			if (isset($instance['image_shape'])) { 
				$image_shape = $instance['image_shape'];
			} else {
				$image_shape = 1;
			}

			$comments = empty( $instance['comments'] ) ? '' : $instance['comments'];

			if (!empty($title))
			  echo $before_title . $title . $after_title;

			$img_shape = '';
			if ($image_shape === 1) {
				$img_shape = 'o-thumb--landscape';
			} elseif ($image_shape === 2) {
				$img_shape = 'o-thumb--portrait';
			} elseif ($image_shape === 3) {
				$img_shape = 'o-thumb--square';
			}

			//$cols = $col_number;

			query_posts('');
			?>

			<div class="brio-popular-posts o-grid">

			<?php
				$popular = new WP_Query( array(
					'showposts' => $number_posts,
					'ignore_sticky_posts' => 1,
					'orderby' => 'comment_count',
					'date_query' => array(
						array(
							'after' => $date_range_posts,
						),
					),
				) );
			?>

			<?php while ( $popular->have_posts() ): $popular->the_post();
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
				if ($thumb) {
					$bg = esc_url($thumb['0']);
				} else {
					$bg = brio_get_first_image();
				}
				$title = get_the_title();

				$col_class = '';
				if ($col_number == 1) {
					$col_class = "bh-col-1";
				} elseif ($col_number == 2) {
					$col_class = "col-6@xs";
				} elseif ($col_number == 3) {
					$col_class = "col-4@xs";
				} elseif ($col_number == 4) {
					$col_class = 'col-6@xs col-3@s';
				} elseif ($col_number == 5) {
					$col_class = 'bh-col-3@xs bh-col-5@s';
				} elseif ($col_number == 6) {
					$col_class = 'col-6@xs col-4@sm col-2@md';
				}

				?>

				<div class="o-grid__item widget-post <?php echo $col_class; ?>">

				<?php 
				if ($display_style === 1) { ?>
					<div class="o-grid uf-jc">

						<div class="o-grid__item col-4 widget-post__image">
							<a href="<?php the_permalink() ?>">
								<div class="o-thumb <?php echo $img_shape;?>">
									<img src="<?php echo $bg; ?>" alt="<?php echo esc_attr($title); ?>" data-pin-nopin="true" />
								</div>
							</a>
						</div>
						<div class="o-grid__item col-8 widget-post__text">
							<h5 class="widget-post__title"><a href="<?php the_permalink() ?>"><?php echo $title; ?></a></h5>
							<?php if(!$comments) {?>
							<p><a href="<?php the_permalink() ?>" class="widget-post__comments-link"><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></a></p>
							<?php } ?>
						</div> 
					</div>
				<?php 
				} elseif ($display_style === 2) { ?>
					<div class="widget-post">
						<a href="<?php the_permalink() ?>" class="widget-post__image-link">
							<div class="o-thumb <?php echo $img_shape;?>">
								<img src="<?php echo $bg; ?>" alt="<?php echo esc_attr($title); ?>" data-pin-nopin="true" />
							</div>
						</a>
						<h5 class="widget-post__title"><a href="<?php the_permalink() ?>"><?php echo $title; ?></a></h5>
						<?php if(!$comments) {?>
						<p><a href="<?php the_permalink() ?>" class="widget-post__comments-link"><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></a></p>
						<?php } ?>
					</div>
				<?php 
				} elseif ($display_style === 3) { ?>
					<div class="o-text-image">
						<a href="<?php the_permalink() ?>">
							<div class="o-thumb <?php echo $img_shape;?>">
								<img src="<?php echo $bg; ?>" alt="<?php echo esc_attr($title); ?>" data-pin-nopin="true" />
							</div>
						</a>
						<div class="o-text-image__overlay">
							<div class="widget-post-textbox">
								<h5 class="widget-post__title"><a href="<?php the_permalink() ?>"><?php echo $title; ?></a></h5>
								<?php if(!$comments) {?>
								<p><a href="<?php the_permalink() ?>" class="widget-post__comments-link"><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></a></p>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php }

				echo '</div>';

			
			endwhile; 
			wp_reset_query(); ?>

			</div>

			<?php
			echo $after_widget;
		}

	}
	
}
