<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Image Upload Widget
 */
class brio_widget_promo_box extends WP_Widget {

	// Holds widget settings defaults, populated in constructor.
	protected $defaults;

	// Constructor. Set the default widget options and create widget.
	public function __construct() {

		// $this->defaults = array(
		// 	'title' => '',
		// 	'subtitle' => '',
		// 	'image_uri' => '',
		// 	'image_shape' => '',
		// 	'link' => '',
		// 	'target' => '',
		// );

		$widget_ops = array(
			'classname' => 'brio-promo-box-widget',
			'description' => __('Create a call to action incorporating an image, title and subtitle.', 'brio-helper'),
		);

		$control_ops = array(
			'width'   => 200,
			'height'  => 250,
		);

		parent::__construct('brio_widget_promo_box', __('Brio Helper: Promo Box', 'brio-helper'), $widget_ops, $control_ops);

	}

	public function form($instance) {

		// Merge with defaults
		//$instance = wp_parse_args((array) $instance, $this->defaults);

		$instance = wp_parse_args( (array) $instance, array( 
			'title' => '', 
			'subtitle' => '',
			'image_uri' => '',
			'image_shape' => 1,
			'link' => '',
			'target' => '',
		));

		$title = $instance['title'];
		$subtitle = $instance['subtitle'];
		$image_uri = $instance['image_uri'];
		$image_shape = absint( $instance['image_shape'] ); 
		$link = $instance['link'];
		$target = $instance['target'];
		?>


		<p>
			<div class="brio-media-container">
				<div class="brio-media-inner">
					<?php $img_style = ($instance[ 'image_uri' ] != '') ? 'margin-bottom:10px;' : 'display:none;'; ?>
					<img id="<?php echo $this->get_field_id('image_uri'); ?>-preview" src="<?php echo esc_attr($instance['image_uri']); ?>" style="max-width: 100%; height: auto;<?php echo $img_style; ?>" />
					<?php $no_img_style = ($instance[ 'image_uri' ] != '') ? 'style="display:none;"' : ''; ?>
				</div>

				<input type="text" id="<?php echo $this->get_field_id('image_uri'); ?>" name="<?php echo $this->get_field_name('image_uri'); ?>" value="<?php echo esc_attr($instance['image_uri']); ?>" class="brio-media-url" style="display: none" />

				<input type="button" value="<?php echo esc_attr(__('Remove', 'brio-helper')); ?>" class="button brio-media-remove" id="<?php echo $this->get_field_id('image_uri'); ?>-remove" style="<?php echo $img_style; ?>" />

				<input type="button" value="<?php echo esc_attr(__('Select Image', 'brio-helper')); ?>" class="button brio-media-upload" id="<?php echo $this->get_field_id('image_uri'); ?>-button" />
				<br class="clear">
			</div>
		</p>

		<p>
			<legend><h3><?php _e('Image shape:', ''); ?></h3></legend>
			<input type="radio" id="<?php echo $this->get_field_id('image_shape_1'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="1" <?php checked( $image_shape == 1, true) ?>>
			<label for="<?php echo $this->get_field_id('image_shape_1'); ?>"><?php _e('Landscape');?></label><br>
			<input type="radio" id="<?php echo $this->get_field_id('image_shape_2'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="2" <?php checked( $image_shape == 2, true) ?>>
			<label for="<?php echo $this->get_field_id('image_shape_2'); ?>"><?php _e('Portrait');?></label><br>
			<input type="radio" id="<?php echo $this->get_field_id('image_shape_3'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="3" <?php checked( $image_shape == 3, true) ?>>
			<label for="<?php echo $this->get_field_id('image_shape_3'); ?>"><?php _e('Square');?></label><br>
			<input type="radio" id="<?php echo $this->get_field_id('image_shape_4'); ?>" name="<?php echo ($this->get_field_name( 'image_shape' )) ?>" value="4" <?php checked( $image_shape == 4, true) ?>>
			<label for="<?php echo $this->get_field_id('image_shape_4'); ?>"><?php _e('Ultra Narrow Landscape');?></label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('subtitle'); ?>"><?php _e('Subtitle (optional!):'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id('subtitle'); ?>" name="<?php echo $this->get_field_name('subtitle'); ?>" value="<?php echo esc_attr($instance['subtitle']); ?>" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Link to open when clicked: (optional)', 'brio-helper'); ?></label>
			<input type="url" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" value="<?php echo esc_attr($instance['link']); ?>" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('target'); ?>">
			<input type="checkbox" id="<?php echo $this->get_field_id('target'); ?>" name="<?php echo $this->get_field_name('target'); ?>" <?php if (isset($instance['target'])) { checked((bool) $instance['target'], true); } ?> /><?php _e('Open link in a new window', 'brio-helper'); ?></label>
		</p>

		<?php

	}

		// Update a particular instance.
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['subtitle'] = strip_tags( $new_instance['subtitle'] );
		$instance['image_uri'] = strip_tags( $new_instance['image_uri'] );
		$instance['image_shape'] = ( isset( $new_instance['image_shape'] ) && $new_instance['image_shape'] > 0 && $new_instance['image_shape'] < 5 ) ? (int) $new_instance['image_shape'] : 0;
		$instance['link'] = strip_tags( $new_instance['link'] );
		$instance['target'] = strip_tags($new_instance['target']);
		return $instance;
	}

	public function widget($args, $instance) {

		$title = empty( $instance['title'] ) ? '' : $instance['title'];
		$subtitle = empty( $instance['subtitle'] ) ? '' : $instance['subtitle'];
		$image_uri = empty( $instance['image_uri'] ) ? '' : $instance['image_uri'];
		$image_shape = empty( $instance['image_shape'] ) ? 1 : $instance['image_shape'];
		$link = empty( $instance['link'] ) ? '' : $instance['link'];
		$target = empty( $instance['target'] ) ? '_self' : $instance['target'];

		$img_shape = '';
		if ($image_shape === 1) {
			$img_shape = 'o-thumb--landscape';
		} elseif ($image_shape === 2) {
			$img_shape = 'o-thumb--portrait';
		} elseif ($image_shape === 3) {
			$img_shape = 'o-thumb--square';
		} elseif ($image_shape === 4) {
			$img_shape = 'o-thumb--narrow';
		}

		echo $args['before_widget'];

		$target = $link_open = $link_close = '';
		if (!empty($instance['link'])) {
			if (!empty($instance['target'])) {
				$target = 'target="_blank"';
			}
			$link_open = '<a href="'.esc_url($instance['link']).'" '.$target.'>';
			$link_close = '</a>';
		}

		echo $link_open;
		echo '<div class="o-text-image c-cta__item">';

			if (!empty($instance['image_uri'])) {
				$image_src = $instance['image_uri'];
				$image_data = brio_helper_get_attachment_id($instance['image_uri']); // use the medium thumbnail if we can find it
				if ($image_data) {
					$image_src = wp_get_attachment_image_src($image_data, 'large');
					$image_src = reset($image_src); // php <5.4 way to get [0] value of array
					$image_src = str_replace('http:', '', $image_src);
				}
				echo '<div class="o-thumb '.$img_shape.'"><img src="'.esc_url($image_src).'" alt="" data-pin-nopin="true" /></div>';
			}

			echo '<div class="o-text-image__overlay c-cta__overlay">';

				if (!empty($instance['subtitle']))
					echo '<span class="c-cta__byline">'.$instance['subtitle'].'</span>';

				if (!empty($instance['title']))
					echo '<span class="c-cta__title">'. $instance['title'] . '</span>';

			echo '</div>';

		echo '</div>';
		echo $link_close.$args['after_widget'];

	}


}
