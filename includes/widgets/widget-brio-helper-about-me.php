<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Image Upload Widget
 */
class brio_aboutme_widget extends WP_Widget {

	// Holds widget settings defaults, populated in constructor.
	protected $defaults;

	function __construct() {

		$this->defaults = array(
			'title' => '',
			'image_uri' => '',
			'bio' => '',
			'rounded' => '',
			'padding' => 0,
		);

		$widget_ops = array(
			'classname' => 'bh-about-me-widget',
			'description' => __('Create a personal profile, including photo, title and biography.', 'brio-helper'),
		);

		$control_ops = array(
			'id_base' => 'brio_image_widget',
			'width'   => 385,
			'height'  => 385,
		);

		parent::__construct('brio_image_widget', __('Brio Helper: About Me', 'brio-helper'), $widget_ops, $control_ops);

	}

	// The settings update form.
	public function form($instance) {

		// Merge with defaults
		$instance = wp_parse_args((array) $instance, $this->defaults);

		?>


		<p>
			<div class="brio-media-container">
				<div class="brio-media-inner" style="box-sizing:border-box;">
					<?php $img_style = ($instance[ 'image_uri' ] != '') ? '' : 'display:none;'; ?>
					<?php
					$rounded = '';
					if (!empty($instance['rounded'])) {
						$rounded = '-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;';
					}
					?>
					<?php
					$paddingAdd = '';
					if (!empty($instance['padding'])) {
						$paddingAdd = 'padding:0 '.$instance['padding'].'px;';
					}
					?>
					<img id="<?php echo $this->get_field_id('image_uri'); ?>-preview" src="<?php echo esc_attr($instance['image_uri']); ?>" style="box-sizing:inherit;max-width: 100%; height: auto;<?php echo $paddingAdd.$rounded.$img_style; ?>" />
					<?php $no_img_style = ($instance[ 'image_uri' ] != '') ? 'style="display:none;"' : ''; ?>
				</div>

				<input type="text" id="<?php echo $this->get_field_id('image_uri'); ?>" name="<?php echo $this->get_field_name('image_uri'); ?>" value="<?php echo esc_attr($instance['image_uri']); ?>" class="brio-media-url" style="display: none" />

				<input type="button" value="<?php echo esc_attr(__('Remove', 'brio-helper')); ?>" class="button brio-media-remove" id="<?php echo $this->get_field_id('image_uri'); ?>-remove" style="<?php echo $img_style; ?>" />

				<input type="button" value="<?php echo esc_attr(__('Select Image', 'brio-helper')); ?>" class="button brio-media-upload" id="<?php echo $this->get_field_id('image_uri'); ?>-button" />
				<br class="clear">
			</div>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('padding'); ?>"><?php _e('Add white space (padding) on left & right sides of image (in pixels):'); ?></label><br>
			<input type="number" id="<?php echo $this->get_field_id('padding'); ?>" name="<?php echo $this->get_field_name('padding'); ?>" value="<?php echo esc_attr($instance['padding']); ?>" max="50" style="width: 50px;" /> px
		<p>
			<label for="<?php echo $this->get_field_id('rounded'); ?>">
			<input type="checkbox" id="<?php echo $this->get_field_id('rounded'); ?>" name="<?php echo $this->get_field_name('rounded'); ?>" <?php if (isset($instance['rounded'])) { checked((bool) $instance['rounded'], true); } ?> /><?php _e('Round the images edges when shown', 'brio-helper'); ?>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('bio'); ?>"><?php _e('Biography', 'brio-helper'); ?></label>
			<textarea id="<?php echo $this->get_field_id('bio'); ?>" name="<?php echo $this->get_field_name('bio'); ?>" class="widefat"><?php if (isset($instance['bio'])) echo wp_kses_post($instance['bio']); ?></textarea>
		</p>



		<?php

	}

	public function update($new_instance, $old_instance) {

		$new_instance['title'] = strip_tags($new_instance['title']);
		$new_instance['image_uri'] = strip_tags($new_instance['image_uri']);
		$new_instance['bio'] = wp_kses_post($new_instance['bio']);
		$new_instance['rounded'] = strip_tags($new_instance['rounded']);
		$new_instance['padding'] = absint($new_instance['padding']);

		return $new_instance;
	}

	public function widget($args, $instance) {

		$instance = wp_parse_args((array) $instance, $this->defaults);

		echo $args['before_widget'];

			$rounded = '';
			if (!empty($instance['rounded'])) {
				$rounded = '-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;';
			}
			$padding = '';
			if (! empty($instance['padding'])) {
				$padding = 'padding:0 '.$instance['padding'].'px;"';
			}

			if (!empty($instance['image_uri'])) {
				$image_src = $instance['image_uri'];
				$image_data = brio_helper_get_attachment_id($instance['image_uri']); // use the medium thumbnail if we can find it
				if ($image_data) {
					$image_src = wp_get_attachment_image_src($image_data, 'large');
					$image_src = reset($image_src); // php <5.4 way to get [0] value of array
					$image_src = str_replace('http:', '', $image_src);
				}
				echo '<img src="'.esc_url($image_src).'" alt="'.$instance['padding'].'" class="profile-image" style="'.$rounded.$padding.'" data-pin-nopin="true" />';
			}

			if (! empty($instance['title']))
				echo $args['before_title'] . apply_filters('widget_title', $instance['title'], $instance, $this->id_base) . $args['after_title'];

			if (!empty($instance['bio']))
				echo wpautop(do_shortcode($instance['bio']));

		echo $args['after_widget'];

	}


}
