<?php

/**
 * Loads the shortcodes.
 *
 * @link       https://brioblogstudio.com
 * @since      0.14
 *
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 */

/**
 * Loads the shortcodes.
 *
 * @since      0.14
 * @package    Brio_Helper
 * @subpackage Brio_Helper/includes
 * @author     Brio Blog Studio <support@brioblogstudio.com>
 */
class Brio_Helper_Shortcodes {


	public function __construct(){

    $this->load_dependencies();

	}


  private function load_dependencies(){

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/shortcodes/shortcode-brio-helper-cols.php';

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/shortcodes/shortcode-brio-helper-social.php';

  }



}
